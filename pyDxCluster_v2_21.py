#!/usr/bin/python
#coding:utf-8
###################################################################################
########################### PyDX Cluster v1.2 by HA1EM x###########################
###################################################################################

###################################################################################
from Tkinter import *
import telnetlib, string, sys, os, subprocess, re
import requests
import pygame

from tkColorChooser import askcolor 
from math import pi, sin, cos, atan2, sqrt, radians, log, tan, degrees

version="v2.21"
pygame.mixer.init()
pygame.mixer.music.load("bell.wav")
pygame.mixer.music.play()

try: 
	f = open("pydxcluster.cfg","r")
	settings = eval(f.read())
	f.close()
except:
	settings = {"host": "", "port": "", "user":"","password":"","ch2200":"1","ch660":"1","ch160":"1","ch80":"1","ch60":"1","ch40":"1","ch30":"1","ch20":"1","ch17":"1","ch15":"1","ch12":"1","ch10":"1","ch6":"1","ch4":"1","ch2":"1","ch70":"1","mainwindowx":0,"mainwindowy":25,"detailwindowx":572,"detailwindowy":274,"viewdetails":1,"consolewindowx":0,"consolewindowy":443,"viewconsole":1,"qrzuser":"","qrzpasswd":"","viewobserver":1,"observerwindowx":569,"observerwindowy":497,"obs1":"","obs2":"","obs3":"","obs4":"","obs5":"","obs6":"","obs7":"","obs8":"","obs9":"","obs10":"","usercallsign":"","usergridloc":"","qrzcquser":"","qrzcqpass":"","qthlocuser":"","qthlocpass":"","detailselection":"None","connect_message":"","rbhost":"","rbport":"","rbon":0,"clusterview":0,"beaconview":0,"pskrepview":0, "md_cw":1, "md_rtty":1, "md_psk31":1, "md_psk63":1, "md_psk125":1, "md_jt9":1, "md_jt65":1, "md_sim31":1, "md_opera":1, "md_ssb":1}
	f = open("pydxcluster.cfg", "w")
	f.write(str(settings))
	f.close()
	f = open("pydxcluster.cfg","r")
	settings = eval(f.read())
	f.close()
	
	
################################# color map load ####################################
try: 
	fc = open("default.col","r")
	colorsettings = eval(fc.read())
	fc.close()
except:
	colorsettings = {"clusterbg":"white","clusterfg":"black","consolebg":"black","consolefg":"#dbff12","spotbg":"#a6f687","spotfg":"black","detailbg":"white","detailfg":"black","detailcall":"brown","detailcountry":"red","call1bg":"red","call1fg":"yellow","call2bg":"blue","call2fg":"yellow","call3bg":"darkgreen","call3fg":"yellow","call4bg":"black","call4fg":"yellow","call5bg":"orange","call5fg":"black","call6bg":"yellow","call6fg":"black","call7bg":"lightblue","call7fg":"black","call8bg":"pink","call8fg":"black","call9bg":"purple","call9fg":"yellow","call10bg":"white","call10fg":"red"}
	fc = open("default.col", "w")
	fc.write(str(colorsettings))
	fc.close()
	fc = open("default.col","r")
	colorsettings = eval(fc.read())
	fc.close()
#####################################################################################	

pskrep = 1
pskrepconnected=0


usercallsign=settings['usercallsign']
usergridloc=settings['usergridloc']
qrzcquser=settings['qrzcquser']
qrzcqpass=settings['qrzcqpass']
qthlocuser=settings['qthlocuser']
qthlocpass=settings['qthlocpass']
olddetailselection=detailselection=settings['detailselection']
connect_message=settings['connect_message']
rbhost=settings['rbhost']
rbport=settings['rbport']
reversebeacon=settings['rbon']
clusterview=settings['clusterview']
rbview=settings['beaconview']
pskrepview=settings['pskrepview']

clusterbg=colorsettings["clusterbg"]
clusterfg=colorsettings["clusterfg"]
consolebg=colorsettings["consolebg"]
consolefg=colorsettings["consolefg"]
spotbg=colorsettings["spotbg"]
spotfg=colorsettings["spotfg"]
detailbg=colorsettings["detailbg"]
detailfg=colorsettings["detailfg"]
detailcall=colorsettings["detailcall"]
detailcountry=colorsettings["detailcountry"]
call1bg=colorsettings["call1bg"]
call1fg=colorsettings["call1fg"]
call2bg=colorsettings["call2bg"]
call2fg=colorsettings["call2fg"]
call3bg=colorsettings["call3bg"]
call3fg=colorsettings["call3fg"]
call4bg=colorsettings["call4bg"]
call4fg=colorsettings["call4fg"]
call5bg=colorsettings["call5bg"]
call5fg=colorsettings["call5fg"]
call6bg=colorsettings["call6bg"]
call6fg=colorsettings["call6fg"]
call7bg=colorsettings["call7bg"]
call7fg=colorsettings["call7fg"]
call8bg=colorsettings["call8bg"]
call8fg=colorsettings["call8fg"]
call9bg=colorsettings["call9bg"]
call9fg=colorsettings["call9fg"]
call10bg=colorsettings["call10bg"]
call10fg=colorsettings["call10fg"]
######################### load LICENSE.txt ##########################################
try:
	lic = open("LICENSE.txt","r")
	licence = lic.read()
	lic.close()	
except:
	licence="LICENCE.txt is not read...\nPresent in deirectory?"
#####################################################################################

rootposition="805x440"+"+"+str(settings["mainwindowx"])+"+"+str(settings["mainwindowy"])
root=Tk()
root.title("pyDXCluster "+version+" by HA1EM")
root.configure(bg='#C9C9C9')
root.resizable(0,0)
root.geometry(rootposition)

######################### Global variables definitions ############################
HOST = settings["host"]
PORT = settings["port"]
user = settings["user"]
password = settings["password"]
qrzuser=settings["qrzuser"]
qrzpass=settings["qrzpasswd"]
respone=''
call =''
freq=''
dx=''
utc=''
utc1=''
filename=''
callcountry=''
dxcountry=''
comment=''
adat=''
gridloc=''
heading=""
connected=0
tn=""
kiiras=0
distance=""
connectstatus=0
ch2200=IntVar()
ch660=IntVar()
ch160=IntVar()
ch80=IntVar()
ch60=IntVar()
ch40=IntVar()
ch30=IntVar()
ch20=IntVar()
ch17=IntVar()
ch15=IntVar()
ch12=IntVar()
ch10=IntVar()
ch6=IntVar()
ch4=IntVar()
ch2=IntVar()
ch70=IntVar()

md_cw=IntVar()
md_rtty=IntVar()
md_psk31=IntVar()
md_psk63=IntVar()
md_psk125=IntVar()
md_jt65=IntVar()
md_jt9=IntVar()
md_sim31=IntVar()
md_opera=IntVar()
md_ssb=IntVar()

detail_ablak=settings["viewdetails"]
console_ablak=settings["viewconsole"]
observer_ablak=settings["viewobserver"]
abutwindow=0
prefwindow=0
userwindow=0
_session_key=''
_session_key1=''
_session_key2=''
qrzconnected=0
obs1=settings["obs1"]
obs2=settings["obs2"]
obs3=settings["obs3"]
obs4=settings["obs4"]
obs5=settings["obs5"]
obs6=settings["obs6"]
obs7=settings["obs7"]
obs8=settings["obs8"]
obs9=settings["obs9"]
obs10=settings["obs10"]
alszik=0
A=ord('A');safety=22
kiirtadat=0
reverseconnected=0
rb=""
pin=""
obc3=0
obc4=0
response=""	
response1=""
response2=""
telnet_input=""

###################################################################################

def checkuserloc(locator):
    ####################### loc hossz ellenőrzés ##################
	if len(locator)>3 and len(locator)<7 and len(locator)!=5:
		if len(locator)==4:
			locatorx=locator[0:2]
			if locatorx.isalpha():
				locatorx=locator[2:3]
				if not locatorx.isalpha():
					locatorx=locator[3:4]
					if not locatorx.isalpha():
						return "ok"
					else:
						return "bad"
				else:
					return "bad"
			else:
				return "bad"
		if len(locator)==6:
			locatorx=locator[0:2]
			if locatorx.isalpha():
				locatorx=locator[2:3]
				if not locatorx.isalpha():
					locatorx=locator[3:4]
					if not locatorx.isalpha():
						locatorx=locator[4:7]
						if locatorx.isalpha():
							return"ok"
						else: return "bad"
					else:
						return "bad"
				else:
					return "bad"
			else:
				return "bad"
	else:
		lconsole.insert(END,"Please set CORRECT User Locator!")
		lconsole.yview(END)			
		lconsole.insert(END,"format is 'JN87KO' or 'JN87'")
		lconsole.yview(END)			
		return "bad"

	

def f(z):
    return 10**(-(z-1)/2)*24**(-z/2)
def locator_to_latlong (locator):
	global safety
	locator = locator.upper()
	longit=latit=-90
	lets=re.findall(r'([A-Xa-x])([A-Xa-x])',locator)
	nums=re.findall(r'(\d)(\d)', locator)
	i=tot=0
	val=range(0,safety) 
	for m in val:
		val[m]=None 
	for x,y in lets:
		val[i*2]=(ord(string.upper(x))-A,ord(string.upper(y))-A)
		i+=1
		tot+=1
	i=0
	for x,y in nums:
		val[i*2+1]=(string.atoi(x),string.atoi(y))
		i+=1
		tot+=1
	i=0
	for x,y in val[0:min(tot,safety-1)]:
		longit+=f(i-1)*x
		latit+=f(i-1)*y
		i+=1
	longit*=2

	return latit, longit

def locatordistance(locator1, locator2):
    R = 6371 
    lat1, long1 = locator_to_latlong(locator1)
    lat2, long2 = locator_to_latlong(locator2)
    
    d_lat = radians(lat2) - radians(lat1)
    d_long = radians(long2) - radians(long1)
    
    r_lat1 = radians(lat1)
    r_long1 = radians(long1)
    r_lat2 = radians(lat2)
    r_long2 = radians(long2)
    
    a = sin(d_lat/2) * sin(d_lat/2) + cos(r_lat1) * cos(r_lat2) * sin(d_long/2) * sin(d_long/2)
    c = 2 * atan2(sqrt(a), sqrt(1-a))
    d = R * c 
    
    return d;    
    
def calculate_heading(locator1, locator2):
    lat1, long1 = locator_to_latlong(locator1)
    lat2, long2 = locator_to_latlong(locator2)
    
    r_lat1 = radians(lat1)
    r_lon1 = radians(long1)
    
    r_lat2 = radians(lat2)
    r_lon2 = radians(long2)

    d_lon = radians(long2 - long1)
    
    b = atan2(sin(d_lon)*cos(r_lat2),cos(r_lat1)*sin(r_lat2)-sin(r_lat1)*cos(r_lat2)*cos(d_lon)) # bearing calc
    bd = degrees(b)
    br,bn = divmod(bd+360,360) 
    
    return bn    


def saveconfig():
	global HOST,PORT,user,password,qrzuser,qrzpass
	global ch2200,ch660,ch160,ch80,ch40,ch30,ch20,ch17,ch15,ch12,ch10,ch6,ch4,ch2,ch70
	global md_cw,md_rtty,md_psk31,md_psk63,md_psk125,md_jt65,md_jt9,md_sim31,md_opera,md_ssb
	
	global obs1,obs2,obs3,obs4,obs5,obs6,obs7,obs8,obs9,obs10
	global rbkapcs,reversebeacon,clusterview,beaconview
	x=root.winfo_rootx()-2
	y=root.winfo_rooty()-48
	if detail_ablak==1:
		dx=detail.winfo_rootx()-2
		dy=detail.winfo_rooty()-18
	else:
		dx=detail.winfo_rootx()
		dy=detail.winfo_rooty()
	if console_ablak==1:
		cx=consolewin.winfo_rootx()-2
		cy=consolewin.winfo_rooty()-18
	else:
		cx=consolewin.winfo_rootx()
		cy=consolewin.winfo_rooty()		
	if observer_ablak==1:
		ox=observer.winfo_rootx()-2
		oy=observer.winfo_rooty()-18
	else:
		ox=observer.winfo_rootx()
		oy=observer.winfo_rooty()	

	if rbkapcs==1: reversebeacon=1
	settings = {"host": HOST, "port": str(PORT), "user":user,"password":password,"ch2200":str(ch2200.get()),"ch660":str(ch660.get()),"ch160":str(ch160.get()),"ch80":str(ch80.get()),"ch60":str(ch60.get()),"ch40":str(ch40.get()),"ch30":str(ch30.get()),"ch20":str(ch20.get()),"ch17":str(ch17.get()),"ch15":str(ch15.get()),"ch12":str(ch12.get()),"ch10":str(ch10.get()),"ch6":str(ch6.get()),"ch4":str(ch4.get()),"ch2":str(ch2.get()),"ch70":str(ch70.get()),"mainwindowx":x,"mainwindowy":y,"detailwindowx":dx,"detailwindowy":dy,"viewdetails":detail_ablak,"consolewindowx":cx,"consolewindowy":cy,"viewconsole":console_ablak,"qrzuser":qrzuser,"qrzpasswd":qrzpass,"viewobserver":observer_ablak,"observerwindowx":ox,"observerwindowy":oy,"obs1":obs1,"obs2":obs2,"obs3":obs3,"obs4":obs4,"obs5":obs5,"obs6":obs6,"obs7":obs7,"obs8":obs8,"obs9":obs9,"obs10":obs10,"usercallsign":usercallsign,"usergridloc":usergridloc,"qrzcquser":qrzcquser,"qrzcqpass":qrzcqpass,"qthlocuser":qthlocuser,"qthlocpass":qthlocpass,"detailselection":v.get(),"connect_message":connect_message,"rbhost":rbhost,"rbport":rbport,"rbon":reversebeacon,"clusterview":clusterview,"beaconview":rbview, "pskrepview":pskrepview , "md_cw":str(md_cw.get()), "md_rtty":str(md_rtty.get()), "md_psk31":str(md_psk31.get()), "md_psk63":str(md_psk63.get()), "md_psk125":str(md_psk125.get()), "md_jt9":str(md_jt9.get()), "md_jt65":str(md_jt65.get()), "md_sim31":str(md_sim31.get()), "md_opera":str(md_opera.get()), "md_ssb":str(md_ssb.get())}
	f = open("pydxcluster.cfg", "w")
	f.write(str(settings))
	f.close()	
	colorsettings = {"clusterbg":clusterbg,"clusterfg":clusterfg,"consolebg":consolebg,"consolefg":consolefg,"spotbg":spotbg,"spotfg":spotfg,"detailbg":detailbg,"detailfg":detailfg,"detailcall":detailcall,"detailcountry":detailcountry,"call1bg":call1bg,"call1fg":call1fg,"call2bg":call2bg,"call2fg":call2fg,"call3bg":call3bg,"call3fg":call3fg,"call4bg":call4bg,"call4fg":call4fg,"call5bg":call5bg,"call5fg":call5fg,"call6bg":call6bg,"call6fg":call6fg,"call7bg":call7bg,"call7fg":call7fg,"call8bg":call8bg,"call8fg":call8fg,"call9bg":call9bg,"call9fg":call9fg,"call10bg":call10bg,"call10fg":call10fg}
	fc = open("default.col", "w")
	fc.write(str(colorsettings))
	fc.close()


def teljeskilepes():
	global tn,rb,reverseconnected,connected
	if reverseconnected==1:rb.write("bye\n")
	if connected==1:tn.write("quit\n")
	saveconfig()
	root.destroy()
	detail.destroy()
	consolewin.destroy()
	abut.destroy()
	observer.destroy()
	prefwin.destroy()
	userwin.destroy()


def connect():
	global HOST,PORT,user,password,connected,tn,_session_key,_session_key1,_session_key2,qrzuser,qrzpass,qrzconnected
	global v
	global reversebeacon,rb,reverseconnected
	global pskrep,pskrepconnected,pin

	if usergridloc=='' :
		lconsole.insert(END,"Please set User Locator!")
		lconsole.yview(END)		
		return
	if checkuserloc(usergridloc)=="bad": return
	if usercallsign=='':
		lconsole.insert(END,"Please set User CALLSIGN!")
		lconsole.yview(END)		
		return		

	if qrzconnected==0:
		########################################################### QRZ QUERY SETUP ##################################	
		if v.get()=="QRZ":
			
			if qrzuser!='' and qrzpass!='':
				regex = re.compile("<Key>(.*)</Key>")
				url = '''https://xmldata.qrz.com/xml/current/?username={0}&password={1}'''.format(qrzuser,qrzpass)
				r = requests.get(url, stream=True)
				content = r.text
				if "<error>" in content: 
					qrzconnected=0
				if "<Key>" in content:
					_session_key1=content[content.find("<Key>")+5:content.find("</Key>")]
					qrzconnected=1
				else:
					qrzconnected=0
			else: qrzconnected=0
		
			
		############################################# HAMCALL CONNECT  ###############################################	
		if v.get()=="HAMCALL":
			if qthlocuser!='' and qthlocpass!='':
				regex = re.compile("<Key>(.*)</Key>")
				url = '''http://www.hamqth.com/xml.php?u={0}&p={1}'''.format(qthlocuser,qthlocpass)
				r = requests.get(url, stream=True)
				content = r.text
				if "<error>" in content: 
					lconsole.insert(END,"Error to connect HamQTH.net!")
					lconsole.yview(END)
				if "<session_id>" in content:
					_session_key=content[content.find("<session_id>")+12:content.find("</session_id>")]
					qrzconnected=1
					lconsole.insert(END,"Connected to HamQTH.net")
					lconsole.yview(END)

				else:
					qrzconnected=0
					lconsole.insert(END,"Not connected to HamQTH.net")
					lconsole.yview(END)
			else: qrzconnected=0			
		############################################ QRZCQ CONNECT ###############################################
		
		if v.get()=="QRZCQ":
			if qrzcquser!='' and qrzcqpass!='':
				regex = re.compile("<Key>(.*)</Key>")
				url = '''https://ssl.qrzcq.com/xml/?username={0}&password={1}'''.format(qrzcquser,qrzcqpass)
				r = requests.get(url, stream=True)
				content = r.text
				if "<error>" in content: 
					qrzconnected=0
				if "<Key>" in content:
					_session_key2=content[content.find("<Key>")+5:content.find("</Key>")]
					qrzconnected=1
				else:
					qrzconnected=0
			else: qrzconnected=0		
     ############################################# CLUSTER SERVER CONNECT ################################################		
	
	if user!=''and HOST!='' and PORT!='':
		if connected==0:
			
			try: 
				tn = telnetlib.Telnet(HOST, PORT,3)
			except: 			
				lconsole.insert(END,"Connection error to "+HOST+" Please check connection data!")
				lconsole.yview(END)
				connected=0
				return			
			tn.write(user + "\n")
			respone=tn.read_until("\n",0.8)
			if "ERROR" in respone:
				connected=0
				lconsole.insert(END,"Username error to "+HOST+" Please check username!")
				lconsole.yview(END)
				return

			
			respone=tn.read_until("\n",0.8)
			if "password:" in respone:
				if password:
					tn.write(password + "\n")
				else:
					lconsole.insert(END,"Request password on "+HOST+" Please check password!")
					lconsole.yview(END)
					connected=0
					return					
				respone=tn.read_until("\n",0.5)
				if "ERROR" in respone:
					lconsole.insert(END,"Password incorrect on "+HOST+" Please check password!")
					lconsole.yview(END)
					connected=0
					return
				else:
					connected=1
			else: connected=1

		else:
			tn.write("quit\n")
			connected=0

	respone=""		
############################################### reverse Beacon connect ###############################################
	reverseanswer=0
	if reversebeacon==1:
		if rbhost!="" and rbport!="":
			if reverseconnected==0:
				while (reverseanswer==0):
					
					try: 
						rb = telnetlib.Telnet(rbhost, rbport,3)
					except: 			
						lconsole.insert(END,"Connection error to "+rbhost+" Please check connection data!")
						lconsole.yview(END)
						reverseconnected=0
						return	
					
					respone=rb.read_until("\n",1) #9
					lconsole.insert(END,"Connecting to "+rbhost+":"+rbport+"...please wait...")
					lconsole.yview(END)					

					if "call" in respone:

						rb.write(user+ "\n")

						try:
							respone=rb.read_until("\n",4)
							reverseanswer=len(respone)
						except:
							print "Timeout"


						if "ERROR" in respone:
							lconsole.insert(END,"callsign incorrect on "+rbhost+" Please check password!")
							lconsole.yview(END)
							reverseconnected=0
							return
						else:
							reverseconnected=1
					else: reverseconnected=1

			else:
				rb.write("bye\n")
				rb.close
				reverseconnected=0

		
		
############################################### PSK Reporter connect ###############################################
	if pskrep==1:
		if pskrepconnected==0:
			
			try: 
				pin = telnetlib.Telnet("pskreporter.info", 7300,3)
			except: 			
				lconsole.insert(END,"Connection error to PSKreporter.info! Please check connection data!")
				lconsole.yview(END)
				pskrepconnected=0

				return			
			pin.write(user + "\n")
			respone=pin.read_until("\n",0.5)
			if "ERROR" in respone:
				connected=0
				lconsole.insert(END,"Username error to PSKreporter.info! Please check username!")
				lconsole.yview(END)
				pskrepconnected=0
				return

			
			respone=pin.read_until("\n",0.5)
			if "Please enter your callsign:" in respone:
				if password:
					pin.write(usercallsign)
					
				respone=pin.read_until("\n",0.5)
				if "ERROR" in respone:
					lconsole.insert(END,"callsign incorrect on PSKreporter.info! Please check password!")
					lconsole.yview(END)
					pskrepconnected=0
					return
				else:
					pskrepconnected=1
			else: pskrepconnected=1
		else:
			pin.write("quit\n")
			pskrepconnected=0			


def callsign(callsign):	
	global qrzuser,qrzpass,_session_key,session_key1,_session_key2,qrzconnected
	global v
	alszik=1
	if qrzconnected==1:

			if v.get()=="QRZ":
				
				
				if _session_key1:
					_session = requests.Session()
					url = """https://xmldata.qrz.com/xml/current/?s={0}&callsign={1}""".format(_session_key1, callsign)
					r = _session.get(url)
					if "error" in r.content:
						calldict={"name":"","country":"","fname":"","addr2":""}
						return calldict
								
					if "<name>" in r.content: name=r.content[r.content.find("<name>")+6:r.content.find("</name>")]
					else: name=""
					if "<fname>" in r.content:fname=r.content[r.content.find("<fname>")+7:r.content.find("</fname>")]
					else: fname=""
					if "<addr2>" in r.content:addr2=r.content[r.content.find("<addr2>")+7:r.content.find("</addr2>")]
					else: addr2=""
					if "<country>" in r.content:country=r.content[r.content.find("<country>")+9:r.content.find("</country>")]
					else: country=""
					calldict={"name":name,"country":country,"fname":fname,"addr2":addr2}
					return calldict
					
					if "not found" in r.content:
						calldict="Not found"
						return calldict

				else:
					return
				alszik=0				

			if v.get()=="QRZCQ":
				
				
				if _session_key2:
					_session = requests.Session()
					url = """https://ssl.qrzcq.com/xml/?s={0}&callsign={1}""".format(_session_key2, callsign)
					r = _session.get(url)
					if "error" in r.content:
						calldict={"name":"","country":"","fname":"","addr2":""}
						return calldict
								
					if "<name>" in r.content: name=r.content[r.content.find("<name>")+6:r.content.find("</name>")]
					else: name=""
					if "<qth>" in r.content:fname=r.content[r.content.find("<qth>")+7:r.content.find("</qth>")]
					else: fname=""
					if "<addr2>" in r.content:addr2=r.content[r.content.find("<addr2>")+7:r.content.find("</addr2>")]
					else: addr2=""
					if "<country>" in r.content:country=r.content[r.content.find("<country>")+9:r.content.find("</country>")]
					else: country=""
					calldict={"name":name,"country":country,"fname":fname,"addr2":addr2}
					return calldict
					
					if "not found" in r.content:
						calldict="Not found"
						return calldict

				else:
					return
				alszik=0					

			if v.get()=="HAMCALL":				
				if _session_key:
					_session = requests.Session()
					url = """http://www.hamqth.com/xml.php?id={0}&callsign={1}""".format(_session_key, callsign)
					r = _session.get(url)
					if "error" in r.content:
						calldict={"fname":"","country":"","name":"","addr2":""}
						return calldict
								
					if "<nick>" in r.content:name=r.content[r.content.find("<nick>")+6:r.content.find("</nick>")]
					else: name=""
					if "<adr_city>" in r.content: city=r.content[r.content.find("<adr_city>")+10:r.content.find("</adr_city>")]
					else: city=""					
					if "<country>" in r.content:country=r.content[r.content.find("<country>")+9:r.content.find("</country>")]
					else: country=""					
					if "<adr_name>" in r.content:fname=r.content[r.content.find("<adr_name>")+10:r.content.find("</adr_name>")]
					else: fname=""					
					
					calldict={"fname":fname,"country":country,"name":name,"addr2":city}
					return calldict
					
					if "not found" in r.content:
						calldict="Not found"
						return calldict	
				else:
					a=a
				alszik=0							


def sendcommand(event):
	global connected,tn
	if connected==1:
		tn.write(lconsolcomm.get()+"\n")
		lconsolcomm.delete(0,END)
def sendcomm():
	global connected,tn
	if connected==1:
		tn.write(lconsolcomm.get()+"\n")
		lconsolcomm.delete(0,END)
#################################################### Foablak konfiguracio ##############################################################


group_r1 = LabelFrame(root, text="Connect",bg='#C9C9C9',labelanchor=N)
group_r1.pack(fill=X, expand=1)

rbview_k=IntVar(group_r1)
clusterview_k=IntVar(group_r1)
pskrepview_k=IntVar(group_r1)

connectbtn=Button(group_r1,width=10, text="Connect",font=("Sans",8),command=connect,bg='lightgray')
connectbtn.grid(row=0, column=0, sticky=W,padx=5,pady=5)
rbview_c = Checkbutton(group_r1, text="View Reverse Beacon",variable=rbview_k,font=("Sans",8),bg='#C9C9C9', fg='green')
rbview_c.grid(row=0,sticky=W,padx=220)
clusterview_c = Checkbutton(group_r1, text="View Cluster",variable=clusterview_k,font=("Sans",8),bg='#C9C9C9', fg='blue')
clusterview_c.grid(row=0,sticky=W,padx=120)
pskrepview_c = Checkbutton(group_r1, text="View PSKreporter",variable=pskrepview_k,font=("Sans",8),bg='#C9C9C9', fg='purple')
pskrepview_c.grid(row=0,sticky=W,padx=370)

if clusterview==1:
	clusterview_c.toggle()
if rbview==1:
	rbview_c.toggle()	
if pskrepview==1:
	pskrepview_c.toggle()	

group_rspot = LabelFrame(root, text="Spot",bg='#C9C9C9',labelanchor=N)
group_rspot.pack(fill=X, expand=1)

spotdx=Entry(group_rspot,width=10)
spotdx.grid(row=4, column=0,sticky=W,padx=95)
spotfreq=Entry(group_rspot,width=10)
spotfreq.grid(row=4,column=0,sticky=W,padx=185)
spotcomment=Entry(group_rspot,width=30)
spotcomment.grid(row=4,column=0,sticky=W,padx=275)

cimkespotdx=Label(group_rspot,text="Callsign",fg=spotfg,bg=spotbg,font=("Sans",8))
cimkespotdx.grid(row=3,column=0,sticky=W,padx=116)
cimkespotfreq=Label(group_rspot,text="Frequency",fg=spotfg,bg=spotbg,font=("Sans",8))
cimkespotfreq.grid(row=3,column=0,sticky=W,padx=198)
cimkespotcomm=Label(group_rspot,text="Comment",fg=spotfg,bg=spotbg,font=("Sans",8))
cimkespotcomm.grid(row=3,column=0,sticky=W,padx=360)

group_r2 = LabelFrame(root, text="Spot",bg='#C9C9C9',labelanchor=N)
group_r2.pack(fill=X, expand=1)

line4=Frame(group_r2,height=20,width=379,bg="red")
line4.grid(row=5,column=0,sticky=W,padx=454)

cimkeutc=Label(group_r2,text="UTC",fg="#5F5F5F",bg="#C9C9C9")
cimkeutc.grid(row=5,column=0,sticky=W)
cimkespotter=Label(group_r2,text="Spotter",fg="#5F5F5F",bg="#C9C9C9")
cimkespotter.grid(row=5,column=0,sticky=W,padx=40)
cimkefreq=Label(group_r2,text="Freq",fg="#5F5F5F",bg="#C9C9C9")
cimkefreq.grid(row=5,column=0,sticky=W,padx=165)
cimkecomment=Label(group_r2,text="Comment",fg="#5F5F5F",bg="#C9C9C9")
cimkecomment.grid(row=5,column=0,sticky=W,padx=200)
cimkedx=Label(group_r2,text="DX",fg="white",bg="red",font=("Sans",9,"bold"))
cimkedx.grid(row=5,column=0,sticky=W,padx=456)

cimkegrid=Label(group_r2,text="Grid",fg="white",bg="red",font=("Sans",9,"bold"))
cimkegrid.grid(row=5,column=0,sticky=W,padx=544)
cimkedistance=Label(group_r2,text="Distance",fg="white",bg="red",font=("Sans",9,"bold"))
cimkedistance.grid(row=5,column=0,sticky=W,padx=607)
cimkeheading=Label(group_r2,text="Head",fg="white",bg="red",font=("Sans",9,"bold"))
cimkeheading.grid(row=5,column=0,sticky=W,padx=686)

scrollbar = Scrollbar(group_r2,orient=VERTICAL)

ldx=Listbox(group_r2,width=98,height=11,font=("Courier",10,"bold"),fg=clusterfg,bg=clusterbg,yscrollcommand=scrollbar.set)
ldx.grid(row=6, sticky=W)
scrollbar.grid(row=6,sticky=NS,padx=790)
scrollbar.config(command=ldx.yview)

line3=Frame(group_r2,height=189,width=2,bg="gray")
line3.grid(row=6,column=0,sticky=W,padx=454)
		
group_r3 = LabelFrame(root, text="Band selector",bg='#C9C9C9',labelanchor=N)
group_r3.pack(fill=X, expand=1)


c2200 = Checkbutton(group_r3, text="2200m",variable=ch2200,font=("Sans",8),bg='#C9C9C9')
c2200.grid(row=7,sticky=W,padx=15)
c660 = Checkbutton(group_r3, text="660m",variable=ch660,font=("Sans",8),bg='#C9C9C9')
c660.grid(row=7,sticky=W,padx=77)
c160 = Checkbutton(group_r3, text="160m",variable=ch160,font=("Sans",8),bg='#C9C9C9')
c160.grid(row=7,sticky=W,padx=132)
c80 = Checkbutton(group_r3, text="80m",variable=ch80,font=("Sans",8),bg='#C9C9C9')
c80.grid(row=7,sticky=W,padx=187)
c60 = Checkbutton(group_r3, text="60m",variable=ch60,font=("Sans",8),bg='#C9C9C9')
c60.grid(row=7,sticky=W,padx=235)
c40 = Checkbutton(group_r3, text="40m",variable=ch40,font=("Sans",8),bg='#C9C9C9')
c40.grid(row=7,sticky=W,padx=283)
c30 = Checkbutton(group_r3, text="30m",variable=ch30,font=("Sans",8),bg='#C9C9C9')
c30.grid(row=7,sticky=W,padx=331)
c20 = Checkbutton(group_r3, text="20m",variable=ch20,font=("Sans",8),bg='#C9C9C9')
c20.grid(row=7,sticky=W,padx=379)
c17 = Checkbutton(group_r3, text="17m",variable=ch17,font=("Sans",8),bg='#C9C9C9')
c17.grid(row=7,sticky=W,padx=427)
c15 = Checkbutton(group_r3, text="15m",variable=ch15,font=("Sans",8),bg='#C9C9C9')
c15.grid(row=7,sticky=W,padx=475)
c12 = Checkbutton(group_r3, text="12m",variable=ch12,font=("Sans",8),bg='#C9C9C9')
c12.grid(row=7,sticky=W,padx=523)
c10 = Checkbutton(group_r3, text="10m",variable=ch10,font=("Sans",8),bg='#C9C9C9')
c10.grid(row=7,sticky=W,padx=571)
c6 = Checkbutton(group_r3, text="6m",variable=ch6,font=("Sans",8),bg='#C9C9C9')
c6.grid(row=7,sticky=W,padx=619)
c4 = Checkbutton(group_r3, text="4m",variable=ch4,font=("Sans",8),bg='#C9C9C9')
c4.grid(row=7,sticky=W,padx=660)
c2 = Checkbutton(group_r3, text="2m",variable=ch2,font=("Sans",8),bg='#C9C9C9')
c2.grid(row=7,sticky=W,padx=701)
c70 = Checkbutton(group_r3, text="70cm",variable=ch70,font=("Sans",8),bg='#C9C9C9')
c70.grid(row=7,sticky=W,padx=740)
if settings["ch2200"]=="1":c2200.toggle()
if settings["ch660"]=="1":c660.toggle()
if settings["ch160"]=="1":c160.toggle()
if settings["ch80"]=="1":c80.toggle()
if settings["ch60"]=="1":c60.toggle()
if settings["ch40"]=="1":c40.toggle()
if settings["ch30"]=="1":c30.toggle()
if settings["ch20"]=="1":c20.toggle()
if settings["ch17"]=="1":c17.toggle()
if settings["ch15"]=="1":c15.toggle()
if settings["ch12"]=="1":c12.toggle()
if settings["ch10"]=="1":c10.toggle()
if settings["ch6"]=="1":c6.toggle()
if settings["ch4"]=="1":c4.toggle()
if settings["ch2"]=="1":c2.toggle()
if settings["ch70"]=="1":c70.toggle()





group_r4 = LabelFrame(root, text="Mode selector",bg='#C9C9C9',labelanchor=N)
group_r4.pack(fill=X, expand=1)

m_cw = Checkbutton(group_r4, text="CW",variable=md_cw,font=("Sans",8),bg='#C9C9C9')
m_cw.grid(row=8,sticky=W,padx=15)
m_rtty = Checkbutton(group_r4, text="RTTY",variable=md_rtty,font=("Sans",8),bg='#C9C9C9')
m_rtty.grid(row=8,sticky=W,padx=55)
m_psk31 = Checkbutton(group_r4, text="PSK31",variable=md_psk31,font=("Sans",8),bg='#C9C9C9')
m_psk31.grid(row=8,sticky=W,padx=105)
m_psk63 = Checkbutton(group_r4, text="PSK63",variable=md_psk63,font=("Sans",8),bg='#C9C9C9')
m_psk63.grid(row=8,sticky=W,padx=160)
m_psk125 = Checkbutton(group_r4, text="PSK125",variable=md_psk125,font=("Sans",8),bg='#C9C9C9')
m_psk125.grid(row=8,sticky=W,padx=215)
m_jt9 = Checkbutton(group_r4, text="JT9",variable=md_jt9,font=("Sans",8),bg='#C9C9C9')
m_jt9.grid(row=8,sticky=W,padx=278)
m_jt65 = Checkbutton(group_r4, text="JT65",variable=md_jt65,font=("Sans",8),bg='#C9C9C9')
m_jt65.grid(row=8,sticky=W,padx=316)
m_sim31 = Checkbutton(group_r4, text="SIM31",variable=md_sim31,font=("Sans",8),bg='#C9C9C9')
m_sim31.grid(row=8,sticky=W,padx=360)
m_opera = Checkbutton(group_r4, text="OPERA",variable=md_opera,font=("Sans",8),bg='#C9C9C9')
m_opera.grid(row=8,sticky=W,padx=411)
m_ssb = Checkbutton(group_r4, text="SSB",variable=md_ssb,font=("Sans",8),bg='#C9C9C9')
m_ssb.grid(row=8,sticky=W,padx=466)

if settings["md_cw"]=="1":m_cw.toggle()
if settings["md_rtty"]=="1":m_rtty.toggle()
if settings["md_psk31"]=="1":m_psk31.toggle()
if settings["md_psk63"]=="1":m_psk63.toggle()
if settings["md_psk125"]=="1":m_psk125.toggle()
if settings["md_jt9"]=="1":m_jt9.toggle()
if settings["md_jt65"]=="1":m_jt65.toggle()
if settings["md_sim31"]=="1":m_sim31.toggle()
if settings["md_opera"]=="1":m_opera.toggle()
if settings["md_ssb"]=="1":m_ssb.toggle()

def details_win():
	global detail_ablak
	if detail_ablak==0:
		detail.update()
		detail.deiconify()
		detail_ablak=1
	else:
		detail.withdraw()
		detail_ablak=0
		
def console_win():
	global console_ablak
	if console_ablak==0:
		consolewin.update()
		consolewin.deiconify()
		console_ablak=1
	else:
		consolewin.withdraw()
		console_ablak=0
		
def observer_win():
	global observer_ablak
	if observer_ablak==0:
		observer.update()
		observer.deiconify()
		observer_ablak=1
	else:
		observer.withdraw()
		observer_ablak=0	
		
def preferences_win():
	global prefwindow,alszik
	if prefwindow==0:
		alszik=1
		prefwin.update()
		prefwin.deiconify()
		prefwindow=1
	else:
		prefwin.withdraw()
		prefwindow=0		
		saveconfig()		
		alszik=0
def user_win():
	global userwindow,alszik
	if userwindow==0:
		#alszik=1
		userwin.update()
		userwin.deiconify()
		userwindow=1
	else:
		userwin.withdraw()
		userwindow=0		
		saveconfig()		
		#alszik=0		
		
#################################### Console window #########################################		
detailposition="646x195+"+str(settings["consolewindowx"])+"+"+str(settings["consolewindowy"])
consolewin = Tk()
consolewin.protocol("WM_DELETE_WINDOW", console_win)
consolewin.title("Console")
consolewin.configure(bg='#C9C9C9')
consolewin.geometry(detailposition)
consolewin.resizable(0,0)
btnconsole=Button(consolewin,text="Send command",font=("Sans",8),command=sendcomm)
btnconsole.grid(row=9, column=0, sticky=W,padx=532,pady=5)
if console_ablak==0:consolewin.withdraw()	
lconsole=Listbox(consolewin, width=80,height=10,font=("Sans",9), bg=consolebg,fg=consolefg)
lconsole.grid(row=8,sticky=W,padx=1)

lconsolcomm=Entry(consolewin,width=65,font=("Sans",9), bg=consolebg,fg=consolefg)
lconsolcomm.grid(row=9,sticky=W,pady=5)
lconsolcomm.bind('<Return>', sendcommand)	
#############################################################################################
###################################### Details window #######################################
detailposition="363x240+"+str(settings["detailwindowx"])+"+"+str(settings["detailwindowy"])
detail=Tk()
detail.protocol("WM_DELETE_WINDOW", details_win)
detail.title("View details from QRZ.com")
detail.geometry(detailposition)
detail.configure(bg='#C9C9C9')
if detail_ablak==0:detail.withdraw()
detail.resizable(0,0)

dxlab_cimke=Label(detail,text="Spotter",fg="#5F5F5F",bg="#C9C9C9")
dxlabel=Listbox(detail,width=51,height=5,fg=detailfg,bg=detailbg,font=("Sans",8,"bold"))
dxlab_cimke1=Label(detail,text="DX",fg="#5F5F5F",bg="#C9C9C9")
dxlabel1=Listbox(detail,width=51,height=8,fg=detailfg,bg=detailbg,font=("Sans",8,"bold"))

dxlab_cimke.grid(row=0,column=0,sticky=W,padx=145)
dxlabel.grid(row=1,column=0,sticky=W)
dxlab_cimke1.grid(row=2,column=0,sticky=W,padx=158)
dxlabel1.grid(row=3,column=0,sticky=W)

###############################################################################################
######################################## Observer window ######################################
observerposition="550x255+"+str(settings["observerwindowx"])+"+"+str(settings["observerwindowy"])
observer=Tk()
observer.protocol("WM_DELETE_WINDOW", observer_win)
observer.title("Callsign Hunter")
observer.geometry(observerposition)
observer.configure(bg='#C9C9C9')
if observer_ablak==0:observer.withdraw()
observer.resizable(0,0)

obcimke3_k=IntVar(observer)
obcimke3 = Checkbutton(observer, text="Alarm sound",variable=obcimke3_k,font=("Sans",8),bg='#C9C9C9')
obcimke3.grid(row=0,sticky=W,padx=5)
obcimke4_k=IntVar(observer)
obcimke4 = Checkbutton(observer, text="Bring to front",variable=obcimke4_k,font=("Sans",8),bg='#C9C9C9')
obcimke4.grid(row=0,sticky=W,padx=165)


obcimke=Label(observer,text="Callsigns",fg="#5F5F5F",bg="#C9C9C9")
obcimke.grid(row=1,column=0,sticky=W,padx=55)
obcimke1=Label(observer,text="Last freq",fg="#5F5F5F",bg="#C9C9C9")
obcimke1.grid(row=1,column=0,sticky=W,padx=185)
obcimke2=Label(observer,text="Last time",fg="#5F5F5F",bg="#C9C9C9")
obcimke2.grid(row=1,column=0,sticky=W,padx=280)
obcimke5=Label(observer,text="Last Call",fg="#5F5F5F",bg="#C9C9C9")
obcimke5.grid(row=1,column=0,sticky=W,padx=376)
obcimke6=Label(observer,text="Last Mode",fg="#5F5F5F",bg="#C9C9C9")
obcimke6.grid(row=1,column=0,sticky=W,padx=466)

ob1=Entry(observer,width=20,font=("Sans",9), bg=call1bg,fg=call1fg)
ob1.grid(row=2,column=0,sticky=W)
obf1=Entry(observer,width=10,font=("Sans",9),disabledforeground="black")
obf1.grid(row=2,column=0,sticky=W,padx=170)
obt1=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obt1.grid(row=2,column=0,sticky=W,padx=265)
obcl1=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obcl1.grid(row=2,column=0,sticky=W,padx=360)
obm1=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obm1.grid(row=2,column=0,sticky=W,padx=453)

ob2=Entry(observer,width=20,font=("Sans",9), bg=call2bg,fg=call2fg)
ob2.grid(row=3,column=0,sticky=W)
obf2=Entry(observer,width=10,font=("Sans",9),disabledforeground="black")
obf2.grid(row=3,column=0,sticky=W,padx=170)
obt2=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obt2.grid(row=3,column=0,sticky=W,padx=265)
obcl2=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obcl2.grid(row=3,column=0,sticky=W,padx=360)
obm2=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obm2.grid(row=3,column=0,sticky=W,padx=453)

ob3=Entry(observer,width=20,font=("Sans",9), bg=call3bg,fg=call3fg)
ob3.grid(row=4,column=0,sticky=W)
obf3=Entry(observer,width=10,font=("Sans",9),disabledforeground="black")
obf3.grid(row=4,column=0,sticky=W,padx=170)
obt3=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obt3.grid(row=4,column=0,sticky=W,padx=265)
obcl3=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obcl3.grid(row=4,column=0,sticky=W,padx=360)
obm3=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obm3.grid(row=4,column=0,sticky=W,padx=453)

ob4=Entry(observer,width=20,font=("Sans",9), bg=call4bg,fg=call4fg)
ob4.grid(row=5,column=0,sticky=W)
obf4=Entry(observer,width=10,font=("Sans",9),disabledforeground="black")
obf4.grid(row=5,column=0,sticky=W,padx=170)
obt4=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obt4.grid(row=5,column=0,sticky=W,padx=265)
obcl4=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obcl4.grid(row=5,column=0,sticky=W,padx=360)
obm4=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obm4.grid(row=5,column=0,sticky=W,padx=453)

ob5=Entry(observer,width=20,font=("Sans",9), bg=call5bg,fg=call5fg)
ob5.grid(row=6,column=0,sticky=W)
obf5=Entry(observer,width=10,font=("Sans",9),disabledforeground="black")
obf5.grid(row=6,column=0,sticky=W,padx=170)
obt5=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obt5.grid(row=6,column=0,sticky=W,padx=265)
obcl5=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obcl5.grid(row=6,column=0,sticky=W,padx=360)
obm5=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obm5.grid(row=6,column=0,sticky=W,padx=453)

ob6=Entry(observer,width=20,font=("Sans",9), bg=call6bg,fg=call6fg)
ob6.grid(row=7,column=0,sticky=W)
obf6=Entry(observer,width=10,font=("Sans",9),disabledforeground="black")
obf6.grid(row=7,column=0,sticky=W,padx=170)
obt6=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obt6.grid(row=7,column=0,sticky=W,padx=265)
obcl6=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obcl6.grid(row=7,column=0,sticky=W,padx=360)
obm6=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obm6.grid(row=7,column=0,sticky=W,padx=453)

ob7=Entry(observer,width=20,font=("Sans",9), bg=call7bg,fg=call7fg)
ob7.grid(row=8,column=0,sticky=W)
obf7=Entry(observer,width=10,font=("Sans",9),disabledforeground="black")
obf7.grid(row=8,column=0,sticky=W,padx=170)
obt7=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obt7.grid(row=8,column=0,sticky=W,padx=265)
obcl7=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obcl7.grid(row=8,column=0,sticky=W,padx=360)
obm7=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obm7.grid(row=8,column=0,sticky=W,padx=453)

ob8=Entry(observer,width=20,font=("Sans",9), bg=call8bg,fg=call8fg)
ob8.grid(row=9,column=0,sticky=W)
obf8=Entry(observer,width=10,font=("Sans",9),disabledforeground="black")
obf8.grid(row=9,column=0,sticky=W,padx=170)
obt8=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obt8.grid(row=9,column=0,sticky=W,padx=265)
obcl8=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obcl8.grid(row=9,column=0,sticky=W,padx=360)
obm8=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obm8.grid(row=9,column=0,sticky=W,padx=453)

ob9=Entry(observer,width=20,font=("Sans",9), bg=call9bg,fg=call9fg)
ob9.grid(row=10,column=0,sticky=W)
obf9=Entry(observer,width=10,font=("Sans",9),disabledforeground="black")
obf9.grid(row=10,column=0,sticky=W,padx=170)
obt9=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obt9.grid(row=10,column=0,sticky=W,padx=265)
obcl9=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obcl9.grid(row=10,column=0,sticky=W,padx=360)
obm9=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obm9.grid(row=10,column=0,sticky=W,padx=453)

ob10=Entry(observer,width=20,font=("Sans",9), bg=call10bg,fg=call10fg)
ob10.grid(row=11,column=0,sticky=W)
obf10=Entry(observer,width=10,font=("Sans",9),disabledforeground="black")
obf10.grid(row=11,column=0,sticky=W,padx=170)
obt10=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obt10.grid(row=11,column=0,sticky=W,padx=265)
obcl10=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obcl10.grid(row=11,column=0,sticky=W,padx=360)
obm10=Entry(observer,width=10,font=("Sans",9),disabledforeground="black", justify="center")
obm10.grid(row=11,column=0,sticky=W,padx=453)

ob1.delete(0,END)
ob1.insert(0,obs1)
ob2.delete(0,END)
ob2.insert(0,obs2)
ob3.delete(0,END)
ob3.insert(0,obs3)
ob4.delete(0,END)
ob4.insert(0,obs4)
ob5.delete(0,END)
ob5.insert(0,obs5)
ob6.delete(0,END)
ob6.insert(0,obs6)
ob7.delete(0,END)
ob7.insert(0,obs7)
ob8.delete(0,END)
ob8.insert(0,obs8)
ob9.delete(0,END)
ob9.insert(0,obs9)
ob10.delete(0,END)
ob10.insert(0,obs10)
obf1.config(state=DISABLED)
obf2.config(state=DISABLED)
obf3.config(state=DISABLED)
obf4.config(state=DISABLED)
obf5.config(state=DISABLED)
obf6.config(state=DISABLED)
obf7.config(state=DISABLED)
obf8.config(state=DISABLED)
obf9.config(state=DISABLED)
obf10.config(state=DISABLED)

obt1.config(state=DISABLED)
obt2.config(state=DISABLED)
obt3.config(state=DISABLED)
obt4.config(state=DISABLED)
obt5.config(state=DISABLED)
obt6.config(state=DISABLED)
obt7.config(state=DISABLED)
obt8.config(state=DISABLED)
obt9.config(state=DISABLED)
obt10.config(state=DISABLED)

obcl1.config(state=DISABLED)
obcl2.config(state=DISABLED)
obcl3.config(state=DISABLED)
obcl4.config(state=DISABLED)
obcl5.config(state=DISABLED)
obcl6.config(state=DISABLED)
obcl7.config(state=DISABLED)
obcl8.config(state=DISABLED)
obcl9.config(state=DISABLED)
obcl10.config(state=DISABLED)

obm1.config(state=DISABLED)
obm2.config(state=DISABLED)
obm3.config(state=DISABLED)
obm4.config(state=DISABLED)
obm5.config(state=DISABLED)
obm6.config(state=DISABLED)
obm7.config(state=DISABLED)
obm8.config(state=DISABLED)
obm9.config(state=DISABLED)
obm10.config(state=DISABLED)
###############################################################################################

root.protocol('WM_DELETE_WINDOW', teljeskilepes)	


def help():
	print
	
	
def about():
	global abutwindow
	if abutwindow==0:
		abut.update()
		abut.deiconify()
		abutwindow=1
	else:
		abut.withdraw()
		abutwindow=0
		
###################################### User  window #####################################
userwinposition="485x400"
userwin=Tk()
userwin.protocol("WM_DELETE_WINDOW", user_win)
userwin.title("user settings")
userwin.geometry(userwinposition)
userwin.configure(bg='#C9C9C9')
if userwindow==0:userwin.withdraw()
userwin.resizable(0,0)		


group1 = LabelFrame(userwin, text="User settings", padx=5, pady=5,bg='#C9C9C9',labelanchor=N)
group1.pack(fill=X, expand=1)


userl1=Label(group1,text="Callsign",bg='#C9C9C9')
userl1.grid(row=0,column=0,sticky=W,padx=5)		
userl1_1=Entry(group1,width=10,bg="white")
userl1_1.grid(row=0,column=0,sticky=W,padx=60)
userl2=Label(group1,text="Locator",bg='#C9C9C9')
userl2.grid(row=0,column=0,sticky=W,padx=150)		
userl2_1=Entry(group1,width=6,bg="white")
userl2_1.grid(row=0,column=0,sticky=W,padx=200)

group2 = LabelFrame(userwin, text="Cluster server settings",padx=5, pady=5,bg='#C9C9C9',labelanchor=N)
group2.pack(fill=X, expand=1)

	
connecthosttext=Label(group2,text="Host:",bg="#C9C9C9")
connecthosttext.grid(row=0,column=0,sticky=W,padx=5)
connecthost=Entry(group2,width=20,disabledbackground="#c8c8c8")
connecthost.grid(row=0,column=0,sticky=W,padx=40)
connectporttext=Label(group2,text="Port:",bg="#C9C9C9")
connectporttext.grid(row=0,column=0,sticky=W,padx=210)
connectport=Entry(group2,width=6,disabledbackground="#c8c8c8")
connectport.grid(row=0,column=0,sticky=W,padx=242)
connectusertext=Label(group2,text="User:",bg="#C9C9C9")
connectusertext.grid(row=1,column=0,sticky=W,padx=5,pady=5)
connectuser=Entry(group2,width=10,disabledbackground="#c8c8c8")
connectuser.grid(row=1,column=0,sticky=W,padx=40)
connectpasswordtext=Label(group2,text="Password:",bg="#C9C9C9")
connectpasswordtext.grid(row=1,column=0,sticky=W,padx=130)
connectpassword=Entry(group2,width=20,show="*",disabledbackground="#c8c8c8")
connectpassword.grid(row=1,column=0,sticky=W,padx=192)
connectmessagetext=Label(group2,text="Default spot message:",bg="#C9C9C9")
connectmessagetext.grid(row=2,column=0,sticky=W,padx=5,pady=5)
connectmessage=Entry(group2,width=30,disabledbackground="#c8c8c8")
connectmessage.grid(row=2,column=0,sticky=W,padx=142)

group3 = LabelFrame(userwin, text="Detail information source",padx=5, pady=5,bg='#C9C9C9',labelanchor=N)
group3.pack(fill=X, expand=1)


qrzuserlabel=Label(group3,text="user:",bg="#C9C9C9")
qrzuserlabel.grid(row=8,column=0,sticky=W,padx=115)
qrzuserentry=Entry(group3,width=10, disabledbackground="#c8c8c8")
qrzuserentry.grid(row=8,column=0,sticky=W,padx=150)
qrzpasslabel=Label(group3,text="password:",bg="#C9C9C9")
qrzpasslabel.grid(row=8,column=0,sticky=W,padx=240)
qrzpassentry=Entry(group3,width=20, show="*", disabledbackground="#c8c8c8")
qrzpassentry.grid(row=8,column=0,sticky=W,padx=305)

qrzcquserlabel=Label(group3,text="user:",bg="#C9C9C9")
qrzcquserlabel.grid(row=9,column=0,sticky=W,padx=115)
qrzcquserentry=Entry(group3,width=10, disabledbackground="#c8c8c8")
qrzcquserentry.grid(row=9,column=0,sticky=W,padx=150)
qrzcqpasslabel=Label(group3,text="password:",bg="#C9C9C9")
qrzcqpasslabel.grid(row=9,column=0,sticky=W,padx=240)
qrzcqpassentry=Entry(group3,width=20, show="*", disabledbackground="#c8c8c8")
qrzcqpassentry.grid(row=9,column=0,sticky=W,padx=305)

hamcalluserlabel=Label(group3,text="user:",bg="#C9C9C9")
hamcalluserlabel.grid(row=10,column=0,sticky=W,padx=115)
hamcalluserentry=Entry(group3,width=10, disabledbackground="#c8c8c8")
hamcalluserentry.grid(row=10,column=0,sticky=W,padx=150)
hamcallpasslabel=Label(group3,text="password:",bg="#C9C9C9")
hamcallpasslabel.grid(row=10,column=0,sticky=W,padx=240)
hamcallpassentry=Entry(group3,width=20, show="*", disabledbackground="#c8c8c8")
hamcallpassentry.grid(row=10,column=0,sticky=W,padx=305)

group4 = LabelFrame(userwin, text="Reverse Beacon settings",padx=5, pady=5,bg='#C9C9C9',labelanchor=N)
group4.pack(fill=X, expand=1)


rbkapcs=IntVar(group4)
rbhosttext=Label(group4,text="Host:",bg="#C9C9C9")
rbhosttext.grid(row=1,column=0,sticky=W,padx=5)
rbhoste=Entry(group4,width=30,disabledbackground="#c8c8c8")
rbhoste.grid(row=1,column=0,sticky=W,padx=40)
rbporttext=Label(group4,text="Port:",bg="#C9C9C9")
rbporttext.grid(row=1,column=0,sticky=W,padx=310)
rbporte=Entry(group4,width=6,disabledbackground="#c8c8c8")
rbporte.grid(row=1,column=0,sticky=W,padx=342)
rbkapcsb = Checkbutton(group4, text="Reverse beacon on/off",variable=rbkapcs,font=("Sans",8),bg='#C9C9C9')
rbkapcsb.grid(row=0,sticky=W,padx=5)
if reversebeacon==1:
	rbkapcsb.toggle()

v = StringVar(userwin)
v.set(detailselection) # initialize

b = Radiobutton(group3, text="None",variable=v, value="None",bg='#C9C9C9')	
b.grid(row=7,column=0,sticky=W)
b1 = Radiobutton(group3, text="QRZ.com",variable=v, value="QRZ",bg='#C9C9C9')	
b1.grid(row=8,column=0,sticky=W)
b2 = Radiobutton(group3, text="QRZCQ.com",variable=v, value="QRZCQ",bg='#C9C9C9')	
b2.grid(row=9,column=0,sticky=W)
b3 = Radiobutton(group3, text="HamQTH.com",variable=v, value="HAMCALL",bg='#C9C9C9')	
b3.grid(row=10,column=0,sticky=W)

userbtn=Button(userwin,width=10, text="Save&Exit",font=("Sans",8),command=user_win,bg='lightgray')
userbtn.pack(fill=X,expand=1)

rbhoste.delete(0,END)
rbhoste.insert(0,rbhost)
rbporte.delete(0,END)
rbporte.insert(0,rbport)

connecthost.delete(0,END)
connecthost.insert(0,HOST)
connectport.delete(0,END)
connectport.insert(0,PORT)
connectuser.delete(0,END)
connectuser.insert(0,user)
connectpassword.delete(0,END)
connectpassword.insert(0,password)
qrzuserentry.delete(0,END)
qrzuserentry.insert(0,qrzuser)
qrzpassentry.delete(0,END)
qrzpassentry.insert(0,qrzpass)

userl1_1.delete(0,END)
userl1_1.insert(0,usercallsign)
userl2_1.delete(0,END)
userl2_1.insert(0,usergridloc)
qrzcquserentry.delete(0,END)
qrzcquserentry.insert(0,qrzcquser)
qrzcqpassentry.delete(0,END)
qrzcqpassentry.insert(0,qrzcqpass)
hamcalluserentry.delete(0,END)
hamcalluserentry.insert(0,qthlocuser)
hamcallpassentry.delete(0,END)
hamcallpassentry.insert(0,qthlocpass)
connectmessage.delete(0,END)
connectmessage.insert(0,connect_message)





###################################### Preferences window #####################################
preferencesposition="235x560"
prefwin=Tk()
prefwin.protocol("WM_DELETE_WINDOW", preferences_win)
prefwin.title("Preferences")
prefwin.geometry(preferencesposition)
prefwin.configure(bg='#C9C9C9')
if prefwindow==0:prefwin.withdraw()
prefwin.resizable(0,0)

def disablebuttons():
	color1.config(state=DISABLED)
	color2.config(state=DISABLED)
	color3.config(state=DISABLED)
	color4.config(state=DISABLED)
	color5.config(state=DISABLED)
	color6.config(state=DISABLED)
	color7.config(state=DISABLED)
	color8.config(state=DISABLED)
	color9.config(state=DISABLED)
	color10.config(state=DISABLED)
	color11.config(state=DISABLED)
	color12.config(state=DISABLED)
	color13.config(state=DISABLED)
	color14.config(state=DISABLED)
	color15.config(state=DISABLED)
	color16.config(state=DISABLED)
	color17.config(state=DISABLED)
	color18.config(state=DISABLED)
	color19.config(state=DISABLED)
	color20.config(state=DISABLED)
	color21.config(state=DISABLED)
	color22.config(state=DISABLED)
	color23.config(state=DISABLED)
	color24.config(state=DISABLED)
	color25.config(state=DISABLED)
	color26.config(state=DISABLED)
	color27.config(state=DISABLED)
	color28.config(state=DISABLED)
	color29.config(state=DISABLED)
	color30.config(state=DISABLED)
	prefok.config(state=DISABLED)
def enablebuttons():
	color1.config(state=NORMAL)
	color2.config(state=NORMAL)
	color3.config(state=NORMAL)
	color4.config(state=NORMAL)
	color5.config(state=NORMAL)
	color6.config(state=NORMAL)
	color7.config(state=NORMAL)
	color8.config(state=NORMAL)
	color9.config(state=NORMAL)
	color10.config(state=NORMAL)
	color11.config(state=NORMAL)
	color12.config(state=NORMAL)
	color13.config(state=NORMAL)
	color14.config(state=NORMAL)
	color15.config(state=NORMAL)
	color16.config(state=NORMAL)
	color17.config(state=NORMAL)
	color18.config(state=NORMAL)
	color19.config(state=NORMAL)
	color20.config(state=NORMAL)
	color21.config(state=NORMAL)
	color22.config(state=NORMAL)
	color23.config(state=NORMAL)
	color24.config(state=NORMAL)
	color25.config(state=NORMAL)
	color26.config(state=NORMAL)
	color27.config(state=NORMAL)
	color28.config(state=NORMAL)
	color29.config(state=NORMAL)
	color30.config(state=NORMAL)
	prefok.config(state=NORMAL)
	
def getColor(number):
	global clusterbg,clusterfg,consolebg,consolefg,spotbg,spotfg,detailbg,detailfg,detailcall,detailcountry
	global call1bg,call1fg,call2bg,call2fg,call3bg,call3fg,call4bg,call4fg,call5bg,call5fg
	global call6bg,call6fg,call7bg,call7fg,call8bg,call8fg,call9bg,call9fg,call10bg,call10fg
	disablebuttons()
	if number==1:
		result = askcolor(color=clusterbg, title = "Change Cluster background color")		
		colbox1.config(state=DISABLED,disabledbackground=result[1])
		ldx.config(bg=result[1])
		clusterbg=result[1]
	if number==2:
		result = askcolor(color=clusterfg, title = "Change Cluster text color")		
		colbox1.config(state=DISABLED,disabledforeground=result[1])		
		ldx.config(fg=result[1])
		clusterfg=result[1]
	if number==3:
		result = askcolor(color=consolebg, title = "Change Console background color")		
		colbox2.config(state=DISABLED,disabledbackground=result[1])		
		lconsole.config(bg=result[1])	
		lconsolcomm.config(bg=result[1])
		consolebg=result[1]	
	if number==4:
		result = askcolor(color=consolefg, title = "Change Console text color")		
		colbox2.config(state=DISABLED,disabledforeground=result[1])		
		lconsole.config(fg=result[1])	
		lconsolcomm.config(fg=result[1])	
		consolefg=result[1]
	if number==5:
		result = askcolor(color=spotbg, title = "Change spot section background color")		
		colbox3.config(state=DISABLED,disabledbackground=result[1])		
		line1.config(bg=result[1])
		line2.config(bg=result[1])
		cimkespotdx.config(bg=result[1])
		cimkespotfreq.config(bg=result[1])
		cimkespotcomm.config(bg=result[1])
		spotbg=result[1]	
	if number==6:
		result = askcolor(color=spotfg, title = "Change Spot section text color")		
		colbox3.config(state=DISABLED,disabledforeground=result[1])		
		cimkespotdx.config(fg=result[1])
		cimkespotfreq.config(fg=result[1])
		cimkespotcomm.config(fg=result[1])
		spotfg=result[1]	
	if number==7:
		result = askcolor(color=detailbg, title = "Change Detail background color")		
		colbox4.config(state=DISABLED,disabledbackground=result[1])		
		dxlabel.config(bg=result[1])
		dxlabel1.config(bg=result[1])
		detailbg=result[1]	
	if number==8:
		result = askcolor(color=detailfg, title = "Change Detail text color")	
		colbox4.config(state=DISABLED,disabledforeground=result[1])				
		dxlabel.config(fg=result[1])
		dxlabel1.config(fg=result[1])
		detailfg=result[1]	
	if number==9:
		result = askcolor(color=detailcall, title = "Change Detail Callsign text color")	
		color9.config(fg=result[1],activeforeground=result[1])	
		detailcall=result[1]	
	if number==10:
		result = askcolor(color=detailcountry, title = "Change  detail Country text color")	
		color10.config(fg=result[1],activeforeground=result[1])	
		detailcountry=result[1]		
	if number==11: #call hunter bgcolor #1
		result = askcolor(color=call1bg, title = "Change Call Hunter Background color")	
		colbox5.config(state=DISABLED,disabledbackground=result[1])	
		ob1.config(bg=result[1])												
		call1bg=result[1]
	if number==12: #call hunter fgcolor #1
		result = askcolor(color=call1fg, title = "Change Call Hunter text color")	
		colbox5.config(state=DISABLED,disabledforeground=result[1])	
		ob1.config(fg=result[1])															
		call1fg=result[1]		
	if number==13: #call hunter bgcolor #2
		result = askcolor(color=call2bg, title = "Change Call Hunter Background color")	
		colbox6.config(state=DISABLED,disabledbackground=result[1])	
		ob2.config(bg=result[1])												
		call2bg=result[1]
	if number==14: #call hunter fgcolor #2
		result = askcolor(color=call2fg, title = "Change Call Hunter text color")	
		colbox6.config(state=DISABLED,disabledforeground=result[1])	
		ob2.config(fg=result[1])															
		call2fg=result[1]	
	if number==15: #call hunter bgcolor #3
		result = askcolor(color=call3bg, title = "Change Call Hunter Background color")	
		colbox7.config(state=DISABLED,disabledbackground=result[1])	
		ob3.config(bg=result[1])												
		call3bg=result[1]
	if number==16: #call hunter fgcolor #3
		result = askcolor(color=call3fg, title = "Change Call Hunter text color")	
		colbox7.config(state=DISABLED,disabledforeground=result[1])	
		ob3.config(fg=result[1])															
		call3fg=result[1]	
	if number==17: #call hunter bgcolor #4
		result = askcolor(color=call4bg, title = "Change Call Hunter Background color")	
		colbox8.config(state=DISABLED,disabledbackground=result[1])	
		ob4.config(bg=result[1])												
		call4bg=result[1]
	if number==18: #call hunter fgcolor #4
		result = askcolor(color=call4fg, title = "Change Call Hunter text color")	
		colbox8.config(state=DISABLED,disabledforeground=result[1])	
		ob4.config(fg=result[1])															
		call4fg=result[1]	
	if number==19: #call hunter bgcolor #5
		result = askcolor(color=call5bg, title = "Change Call Hunter Background color")	
		colbox9.config(state=DISABLED,disabledbackground=result[1])	
		ob5.config(bg=result[1])												
		call5bg=result[1]
	if number==20: #call hunter fgcolor #5
		result = askcolor(color=call5fg, title = "Change Call Hunter text color")	
		colbox9.config(state=DISABLED,disabledforeground=result[1])	
		ob5.config(fg=result[1])															
		call5fg=result[1]	
	if number==21: #call hunter bgcolor #6
		result = askcolor(color=call6bg, title = "Change Call Hunter Background color")	
		colbox10.config(state=DISABLED,disabledbackground=result[1])	
		ob6.config(bg=result[1])												
		call6bg=result[1]
	if number==22: #call hunter fgcolor #6
		result = askcolor(color=call6fg, title = "Change Call Hunter text color")	
		colbox10.config(state=DISABLED,disabledforeground=result[1])	
		ob6.config(fg=result[1])															
		call6fg=result[1]	
	if number==23: #call hunter bgcolor #7
		result = askcolor(color=call7bg, title = "Change Call Hunter Background color")	
		colbox11.config(state=DISABLED,disabledbackground=result[1])	
		ob7.config(bg=result[1])												
		call7bg=result[1]
	if number==24: #call hunter fgcolor #7
		result = askcolor(color=call7fg, title = "Change Call Hunter text color")	
		colbox11.config(state=DISABLED,disabledforeground=result[1])	
		ob7.config(fg=result[1])															
		call7fg=result[1]	
	if number==25: #call hunter bgcolor #8
		result = askcolor(color=call8bg, title = "Change Call Hunter Background color")	
		colbox12.config(state=DISABLED,disabledbackground=result[1])	
		ob8.config(bg=result[1])												
		call8bg=result[1]
	if number==26: #call hunter fgcolor #8
		result = askcolor(color=call8fg, title = "Change Call Hunter text color")	
		colbox12.config(state=DISABLED,disabledforeground=result[1])	
		ob8.config(fg=result[1])															
		call8fg=result[1]	
	if number==27: #call hunter bgcolor #9
		result = askcolor(color=call9bg, title = "Change Call Hunter Background color")	
		colbox13.config(state=DISABLED,disabledbackground=result[1])	
		ob9.config(bg=result[1])												
		call9bg=result[1]
	if number==28: #call hunter fgcolor #9
		result = askcolor(color=call9fg, title = "Change Call Hunter text color")	
		colbox13.config(state=DISABLED,disabledforeground=result[1])	
		ob9.config(fg=result[1])															
		call9fg=result[1]	
	if number==29: #call hunter bgcolor #10
		result = askcolor(color=call10bg, title = "Change Call Hunter Background color")	
		colbox14.config(state=DISABLED,disabledbackground=result[1])	
		ob10.config(bg=result[1])												
		call10bg=result[1]
	if number==30: #call hunter fgcolor #10
		result = askcolor(color=call10fg, title = "Change Call Hunter text color")	
		colbox14.config(state=DISABLED,disabledforeground=result[1])	
		ob10.config(fg=result[1])															
		call10fg=result[1]	
	enablebuttons()
pr_cluster=Label(prefwin,text="Cluster list color",bg='#C9C9C9')
pr_cluster.grid(row=0,column=0,sticky=W,padx=5)				
color1=Button(prefwin,text='Background', command=lambda:getColor(1))
color1.grid(row=1,column=0,sticky=W,padx=55)
color2=Button(prefwin,text='Text', command=lambda:getColor(2))
color2.grid(row=1,column=0,sticky=W,padx=5)
colbox1=Entry(prefwin,width=4)
colbox1.grid(row=1,column=0,sticky=W,padx=160)
colbox1.insert(0,"Text")
colbox1.config(state=DISABLED,disabledbackground=clusterbg,disabledforeground=clusterfg)

pr_console=Label(prefwin,text="Console color",bg='#C9C9C9')
pr_console.grid(row=2,column=0,sticky=W,padx=5)				
color3=Button(prefwin,text='Background', command=lambda:getColor(3))
color3.grid(row=3,column=0,sticky=W,padx=55)
color4=Button(prefwin,text='Text', command=lambda:getColor(4))
color4.grid(row=3,column=0,sticky=W,padx=5)
colbox2=Entry(prefwin,width=4,bg="white")
colbox2.grid(row=3,column=0,sticky=W,padx=160)
colbox2.insert(0,"Text")
colbox2.config(state=DISABLED,disabledbackground=consolebg,disabledforeground=consolefg)

pr_spot=Label(prefwin,text="Spot section color",bg='#C9C9C9')
pr_spot.grid(row=4,column=0,sticky=W,padx=5)				
color5=Button(prefwin,text='Background', command=lambda:getColor(5))
color5.grid(row=5,column=0,sticky=W,padx=55)
color6=Button(prefwin,text='Text', command=lambda:getColor(6))
color6.grid(row=5,column=0,sticky=W,padx=5)
colbox3=Entry(prefwin,width=4)
colbox3.grid(row=5,column=0,sticky=W,padx=160)
colbox3.insert(0,"Text")
colbox3.config(state=DISABLED,disabledbackground=spotbg,disabledforeground="black")

pr_detail=Label(prefwin,text="Detail window color",bg='#C9C9C9')
pr_detail.grid(row=6,column=0,sticky=W,padx=5)				
color7=Button(prefwin,text='Background', command=lambda:getColor(7))
color7.grid(row=7,column=0,sticky=W,padx=55)
color8=Button(prefwin,text='Text', command=lambda:getColor(8))
color8.grid(row=7,column=0,sticky=W,padx=5)
color9=Button(prefwin,fg=detailcall,activeforeground=detailcall,text='Callsign text', command=lambda:getColor(9))
color9.grid(row=8,column=0,sticky=W,padx=5)
color10=Button(prefwin,fg=detailcountry,activeforeground=detailcountry,text='Country text', command=lambda:getColor(10))
color10.grid(row=8,column=0,sticky=W,padx=107)
colbox4=Entry(prefwin,width=4)
colbox4.grid(row=7,column=0,sticky=W,padx=160)
colbox4.insert(0,"Text")
colbox4.config(state=DISABLED,disabledbackground=detailbg,disabledforeground=detailfg)

pr_callhunter=Label(prefwin,text="Callsign Hunter color",bg='#C9C9C9')
pr_callhunter.grid(row=9,column=0,sticky=W,padx=5)				
call_1=Label (prefwin,text="#1",bg='#C9C9C9')
call_1.grid(row=10,column=0,sticky=W,padx=5)
color11=Button(prefwin,text='Background', command=lambda:getColor(11))
color11.grid(row=10,column=0,sticky=W,padx=85)
color12=Button(prefwin,text='Text', command=lambda:getColor(12))
color12.grid(row=10,column=0,sticky=W,padx=35)
colbox5=Entry(prefwin,width=4)
colbox5.grid(row=10,column=0,sticky=W,padx=190)
colbox5.insert(0,"Text")
colbox5.config(state=DISABLED,disabledbackground=call1bg,disabledforeground=call1fg)
#####
call_2=Label (prefwin,text="#2",bg='#C9C9C9')
call_2.grid(row=11,column=0,sticky=W,padx=5)
color13=Button(prefwin,text='Background', command=lambda:getColor(13))
color13.grid(row=11,column=0,sticky=W,padx=85)
color14=Button(prefwin,text='Text', command=lambda:getColor(14))
color14.grid(row=11,column=0,sticky=W,padx=35)
colbox6=Entry(prefwin,width=4)
colbox6.grid(row=11,column=0,sticky=W,padx=190)
colbox6.insert(0,"Text")
colbox6.config(state=DISABLED,disabledbackground=call2bg,disabledforeground=call2fg)
#####
call_3=Label (prefwin,text="#3",bg='#C9C9C9')
call_3.grid(row=12,column=0,sticky=W,padx=5)
color15=Button(prefwin,text='Background', command=lambda:getColor(15))
color15.grid(row=12,column=0,sticky=W,padx=85)
color16=Button(prefwin,text='Text', command=lambda:getColor(16))
color16.grid(row=12,column=0,sticky=W,padx=35)
colbox7=Entry(prefwin,width=4)
colbox7.grid(row=12,column=0,sticky=W,padx=190)
colbox7.insert(0,"Text")
colbox7.config(state=DISABLED,disabledbackground=call3bg,disabledforeground=call3fg)
#####
call_4=Label (prefwin,text="#4",bg='#C9C9C9')
call_4.grid(row=13,column=0,sticky=W,padx=5)
color17=Button(prefwin,text='Background', command=lambda:getColor(17))
color17.grid(row=13,column=0,sticky=W,padx=85)
color18=Button(prefwin,text='Text', command=lambda:getColor(18))
color18.grid(row=13,column=0,sticky=W,padx=35)
colbox8=Entry(prefwin,width=4)
colbox8.grid(row=13,column=0,sticky=W,padx=190)
colbox8.insert(0,"Text")
colbox8.config(state=DISABLED,disabledbackground=call4bg,disabledforeground=call4fg)
#####
call_5=Label (prefwin,text="#5",bg='#C9C9C9')
call_5.grid(row=14,column=0,sticky=W,padx=5)
color19=Button(prefwin,text='Background', command=lambda:getColor(19))
color19.grid(row=14,column=0,sticky=W,padx=85)
color20=Button(prefwin,text='Text', command=lambda:getColor(20))
color20.grid(row=14,column=0,sticky=W,padx=35)
colbox9=Entry(prefwin,width=4)
colbox9.grid(row=14,column=0,sticky=W,padx=190)
colbox9.insert(0,"Text")
colbox9.config(state=DISABLED,disabledbackground=call5bg,disabledforeground=call5fg)
#####
call_6=Label (prefwin,text="#6",bg='#C9C9C9')
call_6.grid(row=15,column=0,sticky=W,padx=5)
color21=Button(prefwin,text='Background', command=lambda:getColor(21))
color21.grid(row=15,column=0,sticky=W,padx=85)
color22=Button(prefwin,text='Text', command=lambda:getColor(22))
color22.grid(row=15,column=0,sticky=W,padx=35)
colbox10=Entry(prefwin,width=4)
colbox10.grid(row=15,column=0,sticky=W,padx=190)
colbox10.insert(0,"Text")
colbox10.config(state=DISABLED,disabledbackground=call6bg,disabledforeground=call6fg)
#####
call_7=Label (prefwin,text="#7",bg='#C9C9C9')
call_7.grid(row=16,column=0,sticky=W,padx=5)
color23=Button(prefwin,text='Background', command=lambda:getColor(23))
color23.grid(row=16,column=0,sticky=W,padx=85)
color24=Button(prefwin,text='Text', command=lambda:getColor(24))
color24.grid(row=16,column=0,sticky=W,padx=35)
colbox11=Entry(prefwin,width=4)
colbox11.grid(row=16,column=0,sticky=W,padx=190)
colbox11.insert(0,"Text")
colbox11.config(state=DISABLED,disabledbackground=call7bg,disabledforeground=call7fg)
#####
call_8=Label (prefwin,text="#8",bg='#C9C9C9')
call_8.grid(row=17,column=0,sticky=W,padx=5)
color25=Button(prefwin,text='Background', command=lambda:getColor(25))
color25.grid(row=17,column=0,sticky=W,padx=85)
color26=Button(prefwin,text='Text', command=lambda:getColor(26))
color26.grid(row=17,column=0,sticky=W,padx=35)
colbox12=Entry(prefwin,width=4)
colbox12.grid(row=17,column=0,sticky=W,padx=190)
colbox12.insert(0,"Text")
colbox12.config(state=DISABLED,disabledbackground=call8bg,disabledforeground=call8fg)
#####
call_9=Label (prefwin,text="#9",bg='#C9C9C9')
call_9.grid(row=18,column=0,sticky=W,padx=5)
color27=Button(prefwin,text='Background', command=lambda:getColor(27))
color27.grid(row=18,column=0,sticky=W,padx=85)
color28=Button(prefwin,text='Text', command=lambda:getColor(28))
color28.grid(row=18,column=0,sticky=W,padx=35)
colbox13=Entry(prefwin,width=4)
colbox13.grid(row=18,column=0,sticky=W,padx=190)
colbox13.insert(0,"Text")
colbox13.config(state=DISABLED,disabledbackground=call9bg,disabledforeground=call9fg)
#####
call_10=Label (prefwin,text="#10",bg='#C9C9C9')
call_10.grid(row=19,column=0,sticky=W,padx=5)
color29=Button(prefwin,text='Background', command=lambda:getColor(29))
color29.grid(row=19,column=0,sticky=W,padx=85)
color30=Button(prefwin,text='Text', command=lambda:getColor(30))
color30.grid(row=19,column=0,sticky=W,padx=35)
colbox14=Entry(prefwin,width=4)
colbox14.grid(row=19,column=0,sticky=W,padx=190)
colbox14.insert(0,"Text")
colbox14.config(state=DISABLED,disabledbackground=call10bg,disabledforeground=call10fg)
#####
prefok=Button(prefwin,text='Save&Exit', command=preferences_win)
prefok.grid(row=20,column=0,sticky=W,padx=80)
###############################################################################################		
####################################### About window setup ##################################

abut=Tk()
abut.title("About")
abut.geometry("480x320")
abut.configure(bg='#C9C9C9')
abut.resizable(0,0)
ab1=Label(abut,text="pyDxCluster",bg="#C9C9C9",font=("Sans",16,"bold"))
ab1.pack()
ab1_1=Label(abut,text="python DX Cluster for HAM Users",bg="#C9C9C9",font=("Sans",12))
ab1_1.pack()
ab2=Label(abut,text="version "+version,bg="#C9C9C9",font=("Sans",10))
ab2.pack()
ab3=Label(abut,text="author: Jakab Szilárd HA1EM",bg="#C9C9C9",font=("Sans",10))
ab3.pack()
ab5=Label(abut,text="published in 11.2014",bg="#C9C9C9",font=("Sans",10))
ab5.pack()
abscrollbar = Scrollbar(abut,orient=VERTICAL)
abscrollbar.pack(side=RIGHT, fill=Y)
ab4=Text(abut,font=("Courier",8),yscrollcommand=abscrollbar.set)
abscrollbar.config(command=ab4.yview)
ab4.pack()
ab4.insert(INSERT,licence)
ab4.config(state=DISABLED)
abut.protocol("WM_DELETE_WINDOW", about)
abut.withdraw()

#############################################################################################
	
	
################################ Menu definitions #################################	
menubar =Menu(root)
root.config(menu=menubar)

filemenu = Menu(menubar, tearoff=0)
filemenu.add_command(label="Save config", command=saveconfig)
filemenu.add_separator()
filemenu.add_command(label="Exit", command=teljeskilepes)

settingsmenu=Menu(menubar,tearoff=0)
settingsmenu.add_command(label="User settings", command=user_win)
settingsmenu.add_command(label="Color settings", command=preferences_win)

viewmenu=Menu(menubar, tearoff=0)
viewmenu.add_command(label="Details show/hide",command=details_win)
viewmenu.add_command(label="Console show/hide",command=console_win)
viewmenu.add_command(label="Callsign hunter show/hide",command=observer_win)


helpmenu = Menu(menubar, tearoff=0)
helpmenu.add_command(label="Help", command=help,state="disabled")
helpmenu.add_command(label="About", command=about)

menubar.add_cascade(label="File", menu=filemenu)
menubar.add_cascade(label="Settings", menu=settingsmenu)
menubar.add_cascade(label="Window", menu=viewmenu)
menubar.add_cascade(label="Help", menu=helpmenu)
####################################################################################


def clearspot():
	spotdx.delete(0,END)
	spotfreq.delete(0,END)
	spotcomment.delete(0,END)

	
def spotting():
	global tn,connected
	dx=spotdx.get()
	freq=spotfreq.get()
	comment=spotcomment.get()
	if connected!=0 and dx!='' and freq:
		if HOST == "cluster.qrzcq.com":
			send="DX "+str(freq)+" "+str(dx)+" "+str(comment)+"\n"
		else:
			send="DX "+str(dx)+" "+str(freq)+" "+str(comment)+"\n"		
		tn.write(send)
		clearspot()

def getfldigi():
	if connected==1:
		filename='fldigi-shell -c log.get_frequency '
		p = subprocess.Popen(filename, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		out, err = p.communicate()
		if out[0:5]=="Error":
			lconsole.insert(END,"FlDigi not connected!")
			lconsole.yview(END)
		else:
			spotfreq.delete(0,END)
			spotfreq.insert(0,str(out[0:len(out)-1]))
			filename='fldigi-shell -c log.get_call '
			p = subprocess.Popen(filename, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
			out, err = p.communicate()
			spotdx.delete(0,END)
			spotdx.insert(0,(str(out[0:len(out)-1])))


def callback():
	global adat,call,freq,dx,utc,comment
	if kiirtadat!=0:
		if ldx.curselection():
			index = ldx.curselection()[0]
			seltext = ldx.get(index)
			freq=''
			call=''
			comment=''
			utc=''
			dx=''
			############# a frekvencia lekerdezese ############
			for letter in seltext[16:24]:
				if (letter!=' '): freq=freq+letter
			############# a spottolt hivojel lekerdezese ######
			for letter in seltext[57:67]:
				if (letter!=' '): dx=dx+letter
			message=connect_message
			spotdx.delete(0,END)
			spotdx.insert(0,dx)
			spotfreq.delete(0,END)
			spotfreq.insert(0,freq)
			spotcomment.delete(0,END)
			spotcomment.insert(0,message)	
			filename='fldigi-shell -d -c "rig.set_frequency '+str(1000*float(freq))+'"'
			p = subprocess.Popen(filename, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
			filename='fldigi-shell -d -c "log.set_call '+dx+'"'
			p = subprocess.Popen(filename, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)	



def detail_viewer(): 
	global detailcall,detailcountry

	global kiirtadat
	if kiirtadat!=0:

		index = ldx.curselection()[0]
		seltext = ldx.get(index)
		gridloc=""
		distance=""
		heading=""
		letter=''
		xcall=""
		alszik=1
		call=""
		dx=""
		############ a spot hivojel lekerdezese ##########
		for letter in seltext[5:17]:
			if (letter!=':' and letter!=' '): call=call+letter

		############# a spottolt hivojel lekerdezese ######
		for letter in seltext[57:67]:
			if (letter!=' '): dx=dx+letter
		############### Grid loc lekerdezese ##############

		if len(seltext)>77:
			for letter in seltext[68:72]:
				gridloc=gridloc+letter		

		if len(gridloc)>0:
			dist=locatordistance(usergridloc,gridloc)
			distance=str(("{0:8.2f}").format(dist))+"km"
			heading=calculate_heading(usergridloc,gridloc)
			heading=("{0:4.1f}").format(float(heading))+"°"
		else: 
			distance=""
			heading=""	


		dxlabel.delete(0,END)
		dxlabel1.delete(0,END)
		dxlabel.insert(0,call)
		dxlabel.itemconfig(0,foreground=detailcall)
		dxlabel1.insert(0,dx)
		dxlabel1.itemconfig(0,foreground=detailcall)

		if call!="--Beacon--":

			called= callsign(call)

			if called :
				if called['name']!="": dxlabel.insert(END,called['name'])
				if called['fname']!="":dxlabel.insert(END,called['fname'])
				if called['country']!="":
					dxlabel.insert(END,called['country'].upper())	
					dxlabel.itemconfig(END,foreground=detailcountry)					
				if called['addr2']!="":dxlabel.insert(END,called['addr2'])
		xcall=""
		letter=""
		for letter in dx:
			if letter=='/' or letter==" ": break
			xcall=xcall+letter
		dx=xcall
		called= callsign(dx)

		if called :
			if called['name']!="":dxlabel1.insert(END,called['name'])
			if called['fname']!="":dxlabel1.insert(END,called['fname'])
			if called['country']!="":
				dxlabel1.insert(END,called['country'].upper())	
				dxlabel1.itemconfig(END,foreground=detailcountry)
			if called['addr2']!="":dxlabel1.insert(END,called['addr2'])
			
		if len(gridloc)!=0:
			dxlabel1.insert(END,"Grid locator: "+gridloc)
			dxlabel1.insert(END,"Distance from you:"+distance)
			dxlabel1.insert(END,"Heading from you :"+heading)
		if called:
			if 'country' in called:
				alszik=0
				return called['country']
						
		alszik=0
spot=Button(group_rspot,text="Send spot",font=("Sans",8),command=spotting)
spot.grid(row=4, column=0, sticky=W,padx=5)
getfl=Button(group_rspot,text="Get FlDigi",font=("Sans",8),command=getfldigi)
getfl.grid(row=4,column=0,sticky=W,padx=531)
getcl=Button(group_rspot,text="Clear",font=("Sans",8),command=clearspot)
getcl.grid(row=4,column=0,sticky=W,padx=720)
getcl=Button(group_rspot,text="Get from list",font=("Sans",8),command=callback)
getcl.grid(row=4,column=0,sticky=W,padx=620)
ldx.bind('<ButtonRelease-1>', lambda event:detail_viewer())


def feldolgoz (response):
	global call,callcountry,dx,dxcountry,freq,comment,utc,gridloc,distance,heading,adat,connected,connectstatus,HOST,PORT,user,password,kiiras
	global ch2200,ch660,ch160,ch80,ch40,ch30,ch20,ch17,ch15,ch12,ch10,ch6,ch4,ch2,ch70
	global md_cw,md_rtty,md_psk31,md_psk63,md_psk128,md_jt65,md_jt9,md_sim31,md_opera,md_ssb
	global dxlabel,qrzuser,qrzpass
	global obs1,obs2,obs3,obs4,obs5,obs6,obs7,obs8,obs9,obs10
	global alszik,kiirtadat
	global v,usercallsign,usergridloc,qrzcquser,qrzcqpass,qthlocuser,qthlocpass,connect_message,olddetailselection,qrzconnected
	global reversebeacon,rbhost,rbport,rbkapcs,rbview_k,rbview,clusterview,clusterview_k
	global obc3,obc4, pin, pskrep, pskrepconnected,telnet_input
	kiiras=0
	mod_kiiras=0;
	adat = response[0:len(response)-2]
	if (len(adat)>=75 and len(adat)<=82) and(adat[0:1]=="D"): 
		call =''
		freq=''
		dx=''
		comment=''
		utc=''
		ido=''
		gridloc=''
		distance=""
		heading=""
		dist=0
		country=''
		freqx=''
		callx=''
		dxx=''
		mode=""
		#print adat
		############# a frekvencia lekerdezese ############
		for letter in adat[15:24]:
			if (letter!=' '): freq=freq+letter	
		freqx=freq
		############# comment lekerdezese #################
		for letter in adat[39:70]:
			comment=comment+letter
		if "CW" in comment.upper(): mode="CW"
		else:
			if "RTTY" in comment.upper(): mode="RTTY"
			else:
				if "PSK31" in comment.upper(): mode="PSK31"
				else:
					if "PSK63" in comment.upper(): mode="PSK63"
					else:
						if "PSK125" in comment.upper(): mode="PSK125"
						else:
							if "JT9" in comment.upper(): mode="JT9"
							else:
								if "JT65" in comment.upper(): mode="JT65"
								else:
									if "SIM31" in comment.upper(): mode="SIM31"																					
									else:
										if "OPERA" in comment.upper(): mode="OPERA"			
										else:
											if "SSB" in comment.upper(): mode="SSB"			
											else:												
												mode="SSB"	
		if 	int(float(freqx))==144489 or int(float(freqx))==70091 or int(float(freqx))==50276 or int(float(freqx))==28076 or int(float(freqx))==24917 or int(float(freqx))==21076 or int(float(freqx))==18102 or int(float(freqx))==14076 or int(float(freqx))==10138 or int(float(freqx))==7076 or int(float(freqx))==5357 or int(float(freqx))==3576 or int(float(freqx))==1838 or int(float(freqx))==474 or int(float(freqx))==136 : mode="JT65"
		
		if mode=="CW" and md_cw.get(): mod_kiiras=1
		if mode=="RTTY" and md_rtty.get(): mod_kiiras=1
		if mode=="PSK31" and md_psk31.get(): mod_kiiras=1
		if mode=="PSK63" and md_psk63.get(): mod_kiiras=1
		if mode=="PSK125" and md_psk125.get(): mod_kiiras=1
		if mode=="JT9" and md_jt9.get(): mod_kiiras=1
		if mode=="JT65" and md_jt65.get(): mod_kiiras=1
		if mode=="SIM31" and md_sim31.get(): mod_kiiras=1
		if mode=="OPERA" and md_opera.get(): mod_kiiras=1
		if mode=="SSB" and md_ssb.get(): mod_kiiras=1
																			

							
		if 	int(float(freqx))>=135 and int(float(freqx))<=137 and ch2200.get(): kiiras=1
		if 	int(float(freqx))>=1810 and int(float(freqx))<=2000 and ch160.get(): kiiras=1
		if 	int(float(freqx))>=3500 and int(float(freqx))<=3800 and ch80.get(): kiiras=1
		if 	int(float(freqx))>=7000 and int(float(freqx))<=7200 and ch40.get(): kiiras=1
		if 	int(float(freqx))>=10100 and int(float(freqx))<=10150 and ch30.get(): kiiras=1
		if 	int(float(freqx))>=14000 and int(float(freqx))<=15000 and ch20.get(): kiiras=1
		if 	int(float(freqx))>=18068 and int(float(freqx))<=18168 and ch17.get(): kiiras=1
		if 	int(float(freqx))>=21000 and int(float(freqx))<=21450 and ch15.get(): kiiras=1
		if 	int(float(freqx))>=24890 and int(float(freqx))<=24990 and ch12.get(): kiiras=1
		if 	int(float(freqx))>=28000 and int(float(freqx))<=30000 and ch10.get(): kiiras=1
		if 	int(float(freqx))>=50000 and int(float(freqx))<=54000 and ch6.get(): kiiras=1
		if 	int(float(freqx))>=70000 and int(float(freqx))<=70500 and ch4.get(): kiiras=1
		if 	int(float(freqx))>=144000 and int(float(freqx))<=146000 and ch2.get(): kiiras=1
		if 	int(float(freqx))>=432000 and int(float(freqx))<=438000 and ch70.get: kiiras=1	


						
		if kiiras==1 and mod_kiiras==1:	
			
			############ a spot hivojel lekerdezese ##########
			for letter in adat[5:17]:
				if (letter!=':' and letter!=' '): call=call+letter
				callx=call
			############# a spottolt hivojel lekerdezese ######
			for letter in adat[25:39]:
				if (letter!=' '): dx=dx+letter
				dxx=dx								

			############## UTC lekerdezese ####################
			for letter in adat[70:74]:
				if (letter!='Z'): utc=utc+letter	
			############### Grid loc lekerdezese ##############
			if len(adat)>77:
				for letter in adat[76:80]:
					gridloc=gridloc+letter

				
			call = ("{0:10s}").format(call)
			freq = ("{0:8.1f}").format(float(freq))
			dx = ("{0:10s}").format(dx)
			comment=("{0:30s}").format(comment)
#						utc=("{0:2.2s}").format(utc)+":"+("{0:2.4s}").format(utc)
			utc1=utc[0:2]+":"+utc[2:4]
#						print utc;
			if len(gridloc)>0:
				dist=locatordistance(usergridloc,gridloc)
				distance=str(("{0:8.2f}").format(dist))+"km"
				heading=calculate_heading(usergridloc,gridloc)
				heading=("{0:4.1f}").format(float(heading))+"°"
			else: 
				distance=""
				heading=""						

			ldx.insert(0,utc+" "+call+" "+freq+" "+comment+" "+dx+" "+gridloc+" "+distance+" "+heading)
			if telnet_input=="c":
				ldx.itemconfig(0,foreground="blue")				
			if telnet_input=="b":
				ldx.itemconfig(0,foreground="green")				
			if telnet_input=="p":
				ldx.itemconfig(0,foreground="purple")	
			
				
			kiirtadat=1
			if "CW" in comment.upper(): mode="CW"
			else:
				if "RTTY" in comment.upper(): mode="RTTY"
				else:
					if "PSK31" in comment.upper(): mode="PSK31"
					else:
						if "PSK63" in comment.upper(): mode="PSK63"
						else:
							if "PSK128" in comment.upper(): mode="PSK128"
							else:
								if "JT9" in comment.upper(): mode="JT9"
								else:
									if "JT65" in comment.upper(): mode="JT65"
									else:
										if "SIM31" in comment.upper(): mode="SIM31"																					
										else:
											if "OPERA" in comment.upper(): mode="OPERA"			
											else:
												if "SSB" in comment.upper(): mode="SSB"			
												else:												
													mode="SSB"
				
			if callx==user.upper() or dxx==user.upper():
				ldx.itemconfig(0,background="lightgreen")	

#			if dxx==obs1.upper():
			if obs1!="" and obs1.upper() in dxx:
				if obc3==1:pygame.mixer.music.play()
				if obc4==1:observer.lift()
				ldx.itemconfig(0,background=call1bg, foreground=call1fg)
				obf1.config(state=NORMAL)
				obf1.delete(0,END)							
				obf1.insert(0,freq)		
				obf1.config(state=DISABLED)	
				obt1.config(state=NORMAL)
				obt1.delete(0,END)							
				obt1.insert(0,utc1)		
				obt1.config(state=DISABLED)	
				obcl1.config(state=NORMAL)
				obcl1.delete(0,END)							
				obcl1.insert(0,dxx)		
				obcl1.config(state=DISABLED)		
				obm1.config(state=NORMAL)
				obm1.delete(0,END)							
				obm1.insert(0,mode)		
				obm1.config(state=DISABLED)																	
			if obs2!="" and obs2.upper() in dxx:
				if obc3==1:pygame.mixer.music.play()							
				if obc4==1:observer.lift()
				ldx.itemconfig(0,background=call2bg, foreground=call2fg)
				obf2.config(state=NORMAL)	
				obf2.delete(0,END)							
				obf2.insert(0,freq)			
				obf2.config(state=DISABLED)	
				obt2.config(state=NORMAL)
				obt2.delete(0,END)							
				obt2.insert(0,utc1)		
				obt2.config(state=DISABLED)		
				obcl2.config(state=NORMAL)
				obcl2.delete(0,END)							
				obcl2.insert(0,dxx)		
				obcl2.config(state=DISABLED)		
				obm2.config(state=NORMAL)
				obm2.delete(0,END)							
				obm2.insert(0,mode)		
				obm2.config(state=DISABLED)															
			if obs3!="" and obs3.upper() in dxx:
				if obc3==1:pygame.mixer.music.play()
				if obc4==1:observer.lift()
				ldx.itemconfig(0,background=call3bg, foreground=call3fg)	
				obf3.config(state=NORMAL)
				obf3.delete(0,END)							
				obf3.insert(0,freq)			
				obf3.config(state=DISABLED)		
				obt3.config(state=NORMAL)
				obt3.delete(0,END)							
				obt3.insert(0,utc1)		
				obt3.config(state=DISABLED)	
				obcl3.config(state=NORMAL)
				obcl3.delete(0,END)							
				obcl3.insert(0,dxx)		
				obcl3.config(state=DISABLED)
				obm3.config(state=NORMAL)
				obm3.delete(0,END)							
				obm3.insert(0,mode)		
				obm3.config(state=DISABLED)																			
			if obs4!="" and obs4.upper() in dxx:
				if obc3==1:pygame.mixer.music.play()							
				if obc4==1:observer.lift()
				ldx.itemconfig(0,background=call4bg, foreground=call4fg)													
				obf4.config(state=NORMAL)
				obf4.delete(0,END)							
				obf4.insert(0,freq)			
				obf4.config(state=DISABLED)		
				obt4.config(state=NORMAL)
				obt4.delete(0,END)							
				obt4.insert(0,utc1)		
				obt4.config(state=DISABLED)	
				obcl4.config(state=NORMAL)
				obcl4.delete(0,END)							
				obcl4.insert(0,dxx)		
				obcl4.config(state=DISABLED)
				obm4.config(state=NORMAL)
				obm4.delete(0,END)							
				obm4.insert(0,mode)		
				obm4.config(state=DISABLED)																			
			if obs5!="" and obs5.upper() in dxx:
				if obc3==1:pygame.mixer.music.play()							
				if obc4==1:observer.lift()
				ldx.itemconfig(0,background=call5bg, foreground=call5fg)							
				obf5.config(state=NORMAL)
				obf5.delete(0,END)							
				obf5.insert(0,freq)			
				obf5.config(state=DISABLED)		
				obt5.config(state=NORMAL)
				obt5.delete(0,END)							
				obt5.insert(0,utc1)		
				obt5.config(state=DISABLED)	
				obcl5.config(state=NORMAL)
				obcl5.delete(0,END)							
				obcl5.insert(0,dxx)		
				obcl5.config(state=DISABLED)
				obm5.config(state=NORMAL)
				obm5.delete(0,END)							
				obm5.insert(0,mode)		
				obm5.config(state=DISABLED)										
			if obs6!="" and obs6.upper() in dxx:
				if obc3==1:pygame.mixer.music.play()							
				if obc4==1:observer.lift()
				ldx.itemconfig(0,background=call6bg, foreground=call6fg)	
				obf6.config(state=NORMAL)
				obf6.delete(0,END)							
				obf6.insert(0,freq)			
				obf6.config(state=DISABLED)		
				obt6.config(state=NORMAL)
				obt6.delete(0,END)							
				obt6.insert(0,utc1)		
				obt6.config(state=DISABLED)					
				obcl6.config(state=NORMAL)
				obcl6.delete(0,END)							
				obcl6.insert(0,dxx)		
				obcl6.config(state=DISABLED)
				obm6.config(state=NORMAL)
				obm6.delete(0,END)							
				obm6.insert(0,mode)		
				obm6.config(state=DISABLED)														
			if obs7!="" and obs7.upper() in dxx:
				if obc3==1:pygame.mixer.music.play()							
				if obc4==1:observer.lift()
				ldx.itemconfig(0,background=call7bg, foreground=call7fg)	
				obf7.config(state=NORMAL)
				obf7.delete(0,END)							
				obf7.insert(0,freq)			
				obf7.config(state=DISABLED)			
				obt7.config(state=NORMAL)
				obt7.delete(0,END)							
				obt7.insert(0,utc1)		
				obt7.config(state=DISABLED)	
				obcl7.config(state=NORMAL)
				obcl7.delete(0,END)							
				obcl7.insert(0,dxx)		
				obcl7.config(state=DISABLED)
				obm7.config(state=NORMAL)
				obm7.delete(0,END)							
				obm7.insert(0,mode)		
				obm7.config(state=DISABLED)																		
			if obs8!="" and obs8.upper() in dxx:
				if obc3==1:pygame.mixer.music.play()							
				if obc4==1:observer.lift()
				ldx.itemconfig(0,background=call8bg, foreground=call8fg)	
				obf8.config(state=NORMAL)
				obf8.delete(0,END)							
				obf8.insert(0,freq)			
				obf8.config(state=DISABLED)			
				obt8.config(state=NORMAL)
				obt8.delete(0,END)							
				obt8.insert(0,utc1)		
				obt8.config(state=DISABLED)	
				obcl8.config(state=NORMAL)
				obcl8.delete(0,END)							
				obcl8.insert(0,dxx)		
				obcl8.config(state=DISABLED)
				obm8.config(state=NORMAL)
				obm8.delete(0,END)							
				obm8.insert(0,mode)		
				obm8.config(state=DISABLED)																		
			if obs9!="" and obs9.upper() in dxx:
				if obc3==1:pygame.mixer.music.play()							
				if obc4==1:observer.lift()
				ldx.itemconfig(0,background=call9bg, foreground=call9fg)	
				obf9.config(state=NORMAL)
				obf9.delete(0,END)							
				obf9.insert(0,freq)			
				obf9.config(state=DISABLED)		
				obt9.config(state=NORMAL)
				obt9.delete(0,END)							
				obt9.insert(0,utc1)		
				obt9.config(state=DISABLED)	
				obcl9.config(state=NORMAL)
				obcl9.delete(0,END)							
				obcl9.insert(0,dxx)		
				obcl9.config(state=DISABLED)
				obm9.config(state=NORMAL)
				obm9.delete(0,END)							
				obm9.insert(0,mode)		
				obm9.config(state=DISABLED)					
																		
			if obs10!="" and obs10.upper() in dxx:
				if obc3==1:pygame.mixer.music.play()							
				if obc4==1:observer.lift()
				ldx.itemconfig(0,background=call10bg, foreground=call10fg)		
				obf10.config(state=NORMAL)
				obf10.delete(0,END)							
				obf10.insert(0,freq)		
				obf10.config(state=DISABLED)	
				obt10.config(state=NORMAL)
				obt10.delete(0,END)							
				obt10.insert(0,utc1)		
				obt10.config(state=DISABLED)
				obcl10.config(state=NORMAL)
				obcl10.delete(0,END)							
				obcl10.insert(0,dxx)		
				obcl10.config(state=DISABLED)																														
				obm10.config(state=NORMAL)
				obm10.delete(0,END)							
				obm10.insert(0,mode)		
				obm10.config(state=DISABLED)					
	else:

		lconsole.insert(END,adat)
		lconsole.yview(END)	
		if adat[0:len(user)]==user.upper():		
			lconsole.itemconfig(END,foreground='red')		

def task():
	global call,callcountry,dx,dxcountry,freq,comment,utc,gridloc,distance,heading,adat,connected,connectstatus,HOST,PORT,user,password,kiiras
	global ch2200,ch660,ch160,ch80,ch40,ch30,ch20,ch17,ch15,ch12,ch10,ch6,ch4,ch2,ch70
	global dxlabel,qrzuser,qrzpass
	global obs1,obs2,obs3,obs4,obs5,obs6,obs7,obs8,obs9,obs10
	global alszik,kiirtadat
	global v,usercallsign,usergridloc,qrzcquser,qrzcqpass,qthlocuser,qthlocpass,connect_message,olddetailselection,qrzconnected
	global reversebeacon,rbhost,rbport,rbkapcs,rbview_k,rbview,clusterview,clusterview_k,pskrepview
	global obc3,obc4, pin, pskrep, pskrepconnected, telnet_input, reverseconnected
	
	response=""	
	response1=""
	response2=""
	telnetflush=""
	telnet_input=""
	adat=''
	if v.get()!=olddetailselection:
		qrzconnected=0
		olddetailselection=v.get()

	if alszik==0:
		if connected==1:
			if connectstatus==0:
				connectbtn.config(text="Disconnect")
				connecthost.config(state='disabled')
				connectport.config(state='disabled')
				connectuser.config(state='disabled')
				connectpassword.config(state='disabled')
				qrzuserentry.config(state='disabled')
				qrzpassentry.config(state='disabled')
				b.config(state='disabled')
				b1.config(state='disabled')
				b2.config(state='disabled')
				b3.config(state='disabled')		
				userl1_1.config(state='disabled')
				userl2_1.config(state='disabled')
				qrzcquserentry.config(state='disabled')
				qrzcqpassentry.config(state='disabled')
				hamcalluserentry.config(state='disabled')
				hamcallpassentry.config(state='disabled')
				connectmessage.configure(state='disabled')
				rbkapcsb.configure(state='disabled')
				rbhoste.configure(state='disabled')
				rbporte.configure(state='disabled')
														
				connectstatus=1

			if clusterview==1:
				response = tn.read_until("\n",0.2)

			else:
				telnetflush=tn.read_until("\n",0.1)
				response=''
			
			if reverseconnected==1:
				if rbview==1:

					response1 = rb.read_until("\n",0.2)

				else:
					telnetflush=rb.read_until("\n",0.1)
					response1=''
			if reverseconnected==0:

					response1=''
######################### PSK Info  ##########################
			if pskrepconnected==1:
				if pskrepview==1:
					response2 = pin.read_until("\n",0.2)
					
				else:
					telnetflush=pin.read_until("\n",0.1)
					response2=''
			if pskrepconnected==0:
					response2=''

##############################################################


			if response!='' or response1!='' or response2!='':
				if response!='':
						response=response
						telnet_input="c"
						feldolgoz(response)				
				if response1!='':
						response=response1
						telnet_input="b"
						feldolgoz(response)
				if response2!='':
						response=response2
						telnet_input="p"					
						feldolgoz(response)
						

			
				
		else:
			if connectstatus==1:
				connectbtn.config(text="Connect")	
				connecthost.config(state='normal')
				connectport.config(state='normal')
				connectuser.config(state='normal')
				connectpassword.config(state='normal')
				qrzuserentry.config(state='normal')
				qrzpassentry.config(state='normal')
				b.config(state='normal')
				b1.config(state='normal')				
				b2.config(state='normal')				
				b3.config(state='normal')		
				userl1_1.config(state='normal')
				userl2_1.config(state='normal')
				qrzcquserentry.config(state='normal')
				qrzcqpassentry.config(state='normal')
				hamcalluserentry.config(state='normal')
				hamcallpassentry.config(state='normal')
				connectmessage.configure(state='normal')		
				rbkapcsb.configure(state='normal')
				rbhoste.configure(state='normal')
				rbporte.configure(state='normal')								

				connectstatus=0	
	HOST=connecthost.get()
	PORT=connectport.get()
	rbhost=rbhoste.get()
	rbport=rbporte.get()
	user=connectuser.get()
	password=connectpassword.get()
	qrzuser=qrzuserentry.get()
	qrzpass=qrzpassentry.get()
	usercallsign=userl1_1.get()
	usergridloc=userl2_1.get()
	qrzcquser=qrzcquserentry.get()
	qrzcqpass=qrzcqpassentry.get()
	qthlocuser=hamcalluserentry.get()
	qthlocpass=hamcallpassentry.get()
	connect_message=connectmessage.get()
	reversebeacon=rbkapcs.get()
	rbview=rbview_k.get()
	clusterview=clusterview_k.get()
	pskrepview=pskrepview_k.get()
	obc3=obcimke3_k.get()
	obc4=obcimke4_k.get()
	
	if ob1.get()!=obs1: 
		obf1.config(state=NORMAL)
		obf1.delete(0,END)	
		obf1.config(state=DISABLED)
		obt1.config(state=NORMAL)
		obt1.delete(0,END)	
		obt1.config(state=DISABLED)		
		obcl1.config(state=NORMAL)
		obcl1.delete(0,END)	
		obcl1.config(state=DISABLED)	
		obm1.config(state=NORMAL)
		obm1.delete(0,END)	
		obm1.config(state=DISABLED)						
	if ob2.get()!=obs2: 
		obf2.config(state=NORMAL)		
		obf2.delete(0,END)	
		obf2.config(state=DISABLED)
		obt2.config(state=NORMAL)
		obt2.delete(0,END)	
		obt2.config(state=DISABLED)		
		obcl2.config(state=NORMAL)
		obcl2.delete(0,END)	
		obcl2.config(state=DISABLED)				
		obm2.config(state=NORMAL)
		obm2.delete(0,END)	
		obm2.config(state=DISABLED)			
	if ob3.get()!=obs3: 
		obf3.config(state=NORMAL)		
		obf3.delete(0,END)	
		obf3.config(state=DISABLED)
		obt3.config(state=NORMAL)
		obt3.delete(0,END)	
		obt3.config(state=DISABLED)	
		obcl3.config(state=NORMAL)
		obcl3.delete(0,END)	
		obcl3.config(state=DISABLED)
		obm3.config(state=NORMAL)
		obm3.delete(0,END)	
		obm3.config(state=DISABLED)								
	if ob4.get()!=obs4: 
		obf4.config(state=NORMAL)		
		obf4.delete(0,END)	
		obf4.config(state=DISABLED)
		obt4.config(state=NORMAL)
		obt4.delete(0,END)	
		obt4.config(state=DISABLED)	
		obcl4.config(state=NORMAL)
		obcl4.delete(0,END)	
		obcl4.config(state=DISABLED)
		obm4.config(state=NORMAL)
		obm4.delete(0,END)	
		obm4.config(state=DISABLED)								
	if ob5.get()!=obs5: 
		obf5.config(state=NORMAL)		
		obf5.delete(0,END)	
		obf5.config(state=DISABLED)		
		obt5.config(state=NORMAL)
		obt5.delete(0,END)	
		obt5.config(state=DISABLED)	
		obcl5.config(state=NORMAL)
		obcl5.delete(0,END)	
		obcl5.config(state=DISABLED)
		obm5.config(state=NORMAL)
		obm5.delete(0,END)	
		obm5.config(state=DISABLED)								
	if ob6.get()!=obs6: 
		obf6.config(state=NORMAL)		
		obf6.delete(0,END)	
		obf6.config(state=DISABLED)		
		obt6.config(state=NORMAL)
		obt6.delete(0,END)	
		obt6.config(state=DISABLED)	
		obcl6.config(state=NORMAL)
		obcl6.delete(0,END)	
		obcl6.config(state=DISABLED)
		obm6.config(state=NORMAL)
		obm6.delete(0,END)	
		obm6.config(state=DISABLED)								
	if ob7.get()!=obs7: 
		obf7.config(state=NORMAL)		
		obf7.delete(0,END)	
		obf7.config(state=DISABLED)		
		obt7.config(state=NORMAL)
		obt7.delete(0,END)	
		obt7.config(state=DISABLED)	
		obcl7.config(state=NORMAL)
		obcl7.delete(0,END)	
		obcl7.config(state=DISABLED)
		obm7.config(state=NORMAL)
		obm7.delete(0,END)	
		obm7.config(state=DISABLED)								
	if ob8.get()!=obs8: 
		obf8.config(state=NORMAL)		
		obf8.delete(0,END)	
		obf8.config(state=DISABLED)		
		obt8.config(state=NORMAL)
		obt8.delete(0,END)	
		obt8.config(state=DISABLED)	
		obcl8.config(state=NORMAL)
		obcl8.delete(0,END)	
		obcl8.config(state=DISABLED)
		obm8.config(state=NORMAL)
		obm8.delete(0,END)	
		obm8.config(state=DISABLED)								
	if ob9.get()!=obs9: 
		obf9.config(state=NORMAL)		
		obf9.delete(0,END)	
		obf9.config(state=DISABLED)		
		obt9.config(state=NORMAL)
		obt9.delete(0,END)	
		obt9.config(state=DISABLED)				
		obcl9.config(state=NORMAL)
		obcl9.delete(0,END)	
		obcl9.config(state=DISABLED)
		obm9.config(state=NORMAL)
		obm9.delete(0,END)	
		obm9.config(state=DISABLED)					
	if ob10.get()!=obs10:
		obf10.config(state=NORMAL)		
		obf10.delete(0,END)	
		obf10.config(state=DISABLED)	
		obt10.config(state=NORMAL)
		obt10.delete(0,END)	
		obt10.config(state=DISABLED)
		obcl10.config(state=NORMAL)
		obcl10.delete(0,END)	
		obcl10.config(state=DISABLED)				
		obm10.config(state=NORMAL)
		obm10.delete(0,END)	
		obm10.config(state=DISABLED)			
	
	obs1=ob1.get()
	obs2=ob2.get()
	obs3=ob3.get()
	obs4=ob4.get()
	obs5=ob5.get()
	obs6=ob6.get()
	obs7=ob7.get()
	obs8=ob8.get()
	obs9=ob9.get()
	obs10=ob10.get()

	kiiras=0
	root.after(1,task)  

root.after(1,task)
root.mainloop()
